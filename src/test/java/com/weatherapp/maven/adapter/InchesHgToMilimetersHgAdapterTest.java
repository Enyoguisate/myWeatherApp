/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weatherapp.maven.adapter;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enyoguisate
 */
public class InchesHgToMilimetersHgAdapterTest {
    
    public InchesHgToMilimetersHgAdapterTest() {
    }
    
    @Test
    public void testGetMilimetersHg() {
        System.out.println("Test getMilimetersHg method");
        InchesHgImpl inchesHgImplMock = EasyMock.createMock(InchesHgImpl.class);
        EasyMock.expect(inchesHgImplMock.getInchesHg(10)).andReturn((double)10);
        EasyMock.replay(inchesHgImplMock);
        InchesHgToMilimetersHgAdapter inchesHgToMilimetersHgAdapter = new InchesHgToMilimetersHgAdapter();
        inchesHgToMilimetersHgAdapter.setInchesHgImpl(inchesHgImplMock);
        double res = inchesHgToMilimetersHgAdapter.getMilimetersHg(10);
        assertEquals((double)250, res, 0.01);
        EasyMock.verify(inchesHgImplMock);
    }
    
}
