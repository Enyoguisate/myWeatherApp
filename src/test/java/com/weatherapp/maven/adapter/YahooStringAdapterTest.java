/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weatherapp.maven.adapter;

import com.weatherapp.maven.entitymodels.Day;
import com.weatherapp.maven.proxy.ProxyWeather;
import java.util.ArrayList;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enyoguisate
 */
public class YahooStringAdapterTest {
    
    public YahooStringAdapterTest() {
    }
   

    @Test
    public void testGetLocation() {
        System.out.println("Test getLocation method");
        ProxyWeather proxyWeatherMock = EasyMock.createMock(ProxyWeather.class);
        EasyMock.expect(proxyWeatherMock.getWeatherResponse(EasyMock.anyString(), EasyMock.anyString())).andReturn("someString");
        EasyMock.replay(proxyWeatherMock);
        YahooStringAdapter yahooStringAdapter = new YahooStringAdapter();
        yahooStringAdapter.setTestProxyWeather(proxyWeatherMock);
        String result = yahooStringAdapter.getLocation("someCountry", "someCity");
        String expected = "someString";
        assertEquals(expected, result);
        EasyMock.verify(proxyWeatherMock);
    }
    
    @Test
    public void testGetAllDays() {
        System.out.println("Test getAllDays method");
        ProxyWeather proxyWeatherMock = EasyMock.createMock(ProxyWeather.class);
        EasyMock.expect(proxyWeatherMock.getDaysList()).andReturn(new ArrayList<>());
        EasyMock.replay(proxyWeatherMock);
        YahooStringAdapter yahooStringAdapter = new YahooStringAdapter();
        yahooStringAdapter.setTestProxyWeather(proxyWeatherMock);
        String result = yahooStringAdapter.getAllDays();
        String expected = "";        
        assertEquals(expected, result);
        EasyMock.verify(proxyWeatherMock);
    }

    @Test
    public void testGetDayById() {
        System.out.println("Test getDayById method");
        ProxyWeather proxyWeatherMock = EasyMock.createMock(ProxyWeather.class);
        EasyMock.expect(proxyWeatherMock.getDaysList()).andReturn(new ArrayList<>());
        EasyMock.replay(proxyWeatherMock);
        YahooStringAdapter yahooStringAdapter = new YahooStringAdapter();
        yahooStringAdapter.setTestProxyWeather(proxyWeatherMock);
        String result = yahooStringAdapter.getDayById(1);
        String expected = "";        
        assertEquals(expected, result);
        EasyMock.verify(proxyWeatherMock);
    }

    @Test
    public void testUpdateDay() {
        System.out.println("updateDay");
        ProxyWeather proxyWeatherMock = EasyMock.createMock(ProxyWeather.class);
        Day dayMock = EasyMock.createMock(Day.class);
        EasyMock.expect(proxyWeatherMock.updateDay(dayMock)).andReturn(new ArrayList<>());
        EasyMock.replay(proxyWeatherMock, dayMock);
        YahooStringAdapter yahooStringAdapter = new YahooStringAdapter();
        yahooStringAdapter.setTestProxyWeather(proxyWeatherMock);
        String result = yahooStringAdapter.updateDay(dayMock);
        String expected = "";        
        assertEquals(expected, result);
        EasyMock.verify(proxyWeatherMock, dayMock);
    }

    @Test
    public void testAddDay() {
        System.out.println("addDay");
        ProxyWeather proxyWeatherMock = EasyMock.createMock(ProxyWeather.class);
        Day dayMock = EasyMock.createMock(Day.class);
        EasyMock.expect(proxyWeatherMock.addDay(dayMock)).andReturn(new ArrayList<>());
        EasyMock.replay(proxyWeatherMock, dayMock);
        YahooStringAdapter yahooStringAdapter = new YahooStringAdapter();
        yahooStringAdapter.setTestProxyWeather(proxyWeatherMock);
        String result = yahooStringAdapter.addDay(dayMock);
        String expected = "";        
        assertEquals(expected, result);
        EasyMock.verify(proxyWeatherMock, dayMock);
    }
    
}
