/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weatherapp.maven.adapter;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enyoguisate
 */
public class MileToKilometerAdapterTest {
    
    public MileToKilometerAdapterTest() {
    }
    

    @Test
    public void testGetKilometer() {
        System.out.println("Test getKilometer method");
        MileImpl mileMock = EasyMock.createMock(MileImpl.class);
        EasyMock.expect(mileMock.getMileDistance(10)).andReturn((double)16);
        EasyMock.replay(mileMock);
        MileToKilometerAdapter mileToKilometerAdapter = new MileToKilometerAdapter();
        mileToKilometerAdapter.setMileDistance(mileMock);
        double res = mileToKilometerAdapter.getKilometer(10);
        assertEquals((double)16, res, 0.01);
        EasyMock.verify(mileMock);
    }
    
}
