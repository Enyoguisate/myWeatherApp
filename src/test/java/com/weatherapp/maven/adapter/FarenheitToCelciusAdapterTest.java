
package com.weatherapp.maven.adapter;

import org.easymock.EasyMock;
import org.junit.Test;
import static org.junit.Assert.*;

public class FarenheitToCelciusAdapterTest {
    public FarenheitToCelciusAdapterTest() {        
    }
    @Test
    public void testGetCelciusTemperature() {
        System.out.println("Test getCelciusTemperature method");
        FarenheitImpl farenheitMock = EasyMock.createMock(FarenheitImpl.class);
        EasyMock.expect(farenheitMock.getFarenheitTemperature(42)).andReturn((double)42);
        EasyMock.replay(farenheitMock);
        FarenheitToCelciusAdapter farenheitAdapter = new FarenheitToCelciusAdapter();
        farenheitAdapter.setFarenheitTemp(farenheitMock);
        double res = farenheitAdapter.getCelciusTemperature(42);
        assertEquals((double)10, res, 0.01);
        EasyMock.verify(farenheitMock);
        
}
    
}
