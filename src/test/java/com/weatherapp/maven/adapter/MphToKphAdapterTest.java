/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weatherapp.maven.adapter;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enyoguisate
 */
public class MphToKphAdapterTest {
    
    public MphToKphAdapterTest() {
    }
    
    
    @Test
    public void testGetKphSpeed() {
        System.out.println("Test getKphSpeed method");
        MphImpl mphMock = EasyMock.createMock(MphImpl.class);
        EasyMock.expect(mphMock.getMphSpeed(10)).andReturn((double)16);
        EasyMock.replay(mphMock);
        MphToKphAdapter mphToKphAdapter = new MphToKphAdapter();
        mphToKphAdapter.setMphImpl(mphMock);
        double res = mphToKphAdapter.getKphSpeed(10);
        assertEquals((double)16, res, 0.01);
        EasyMock.verify(mphMock);
    }
    
}
