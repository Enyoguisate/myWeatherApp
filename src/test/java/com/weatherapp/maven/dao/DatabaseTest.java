
package com.weatherapp.maven.dao;

import java.sql.Connection;
import java.sql.SQLException;
import org.easymock.EasyMock;
import org.junit.Test;
import static org.junit.Assert.*;


public class DatabaseTest {
    
    public DatabaseTest() {
    }
  
    @Test
    public void testGetConnection() throws ClassNotFoundException, SQLException, Exception{
        System.out.println("Test getConnection method");
        String jdbcDriver = "org.h2.Driver";
        String url = "jdbc:h2:~/h2weatherdb";
        String username = "root";
        String password = "";
        Connection connection;
        
        Database dbMock = EasyMock.createMock(Database.class);
        Connection conMock = EasyMock.createMock(Connection.class);
        EasyMock.expect(dbMock.getConnection())
            .andReturn(new Database(jdbcDriver, url, username, password)
                    .getConnection());
        
        EasyMock.replay(dbMock,conMock);
        
        Database dbInstance = new Database(jdbcDriver, url, username, password);
        connection = dbInstance.getConnection();
        conMock = dbMock.getConnection();
        
        assertTrue(connection.getCatalog().contains(conMock.getCatalog()));
        
        EasyMock.verify(dbMock);             
    }
    
}
