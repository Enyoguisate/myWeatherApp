
package com.weatherapp.maven.entitymodels;
import org.junit.Test;
import static org.junit.Assert.*;


public class WindTest {
    
    public WindTest() {
    }
    
    //Test of class Wind.
    
    @Test
    public void testGetId() {
        System.out.println("Test getId method");
        Wind instance = new Wind();
        instance.setId(6);
        assertEquals(6, instance.getId());
    }

    @Test
    public void testSetId() {
        System.out.println("Test setId method");
        Wind instance = new Wind();
        instance.setId(6);
        assertEquals(6,instance.getId());
    }

    @Test
    public void testGetDirection() {
        System.out.println("Test getDirection method");
        Wind instance = new Wind();
        String res = "N";
        instance.setDirection(res);
        assertTrue(instance.getDirection().contains(res));
    }

    @Test
    public void testSetDirection() {
        System.out.println("Test setDirection method");
        String res = "N";
        Wind instance = new Wind();
        instance.setDirection(res);
        assertTrue(instance.getDirection().contains(res));
    }

    @Test
    public void testGetSpeed() {
        System.out.println("Test getSpeed method");
        Wind instance = new Wind();
        String res = "15";
        instance.setSpeed(res);
        assertTrue(instance.getSpeed().contains(res));
    }

    @Test
    public void testSetSpeed() {
        System.out.println("Test setSpeed method");
        String res = "15";
        Wind instance = new Wind();
        instance.setSpeed(res);
        assertTrue(instance.getSpeed().contains(res));
    }


}
