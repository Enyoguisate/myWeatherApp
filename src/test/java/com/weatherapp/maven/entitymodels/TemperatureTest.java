
package com.weatherapp.maven.entitymodels;
import org.junit.Test;
import static org.junit.Assert.*;


public class TemperatureTest {
    
    public TemperatureTest() {
    }
    
    //Test of class Temperature.
     
    @Test
    public void testGetId() {
        System.out.println("Test getId method");
        Temperature instance = new Temperature();
        instance.setId(6);
        assertEquals(6, instance.getId());
    }

    @Test
    public void testSetId() {
        System.out.println("Test setId method");
        Temperature instance = new Temperature();
        instance.setId(6);
        assertEquals(6,instance.getId());
    }

    @Test
    public void testGetTemp() {
        System.out.println("Test getTemp method");
        Temperature instance = new Temperature();
        String res = "67";
        instance.setTemp(res);
        assertTrue(instance.getTemp().contains(res));
    }

    @Test
    public void testSetTemp() {
        System.out.println("Test setTemp method");
        Temperature instance = new Temperature();
        String res= "67";
        instance.setTemp(res);
        assertTrue(instance.getTemp().contains(res));
    }

    @Test
    public void testGetHigh() {
        System.out.println("Test getHigh method");
        Temperature instance = new Temperature();
        String res = "71";
        instance.setHigh(res);
        assertTrue(instance.getHigh().contains(res));
    }

    @Test
    public void testSetHigh() {
        System.out.println("Test setHigh method");
        String res = "71";
        Temperature instance = new Temperature();
        instance.setHigh(res);
        assertTrue(instance.getHigh().contains(res));
    }

    @Test
    public void testGetLow() {
        System.out.println("Test getLow method");
        Temperature instance = new Temperature();
        String res = "61";
        instance.setLow(res);
        assertTrue(instance.getLow().contains(res));
    }
    
    @Test
    public void testSetLow() {
        System.out.println("Test setLow method");
        String res = "61";
        Temperature instance = new Temperature();
        instance.setLow(res);
        assertTrue(instance.getLow().contains(res));
    }

}
