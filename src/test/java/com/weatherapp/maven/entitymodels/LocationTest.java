
package com.weatherapp.maven.entitymodels;
import org.junit.Test;
import static org.junit.Assert.*;

public class LocationTest {
    
    public LocationTest() {
    }
    
    //Test ofclass Location.
    
    @Test
    public void testGetId() {
        System.out.println("Test getId method");
        Location instance = new Location();
        instance.setId(1);
        assertEquals(1, instance.getId());
    }

    @Test
    public void testSetId() {
        System.out.println("Test setId method method");
        Location instance = new Location();
        instance.setId(1);
        assertEquals(1,instance.getId());
    }

    @Test
    public void testGetCity() {
        System.out.println("Test getCity method method");
        Location instance = new Location();
        String res = "Las Vegas";
        instance.setCity(res);
        assertTrue(instance.getCity().contains(res));
    }

    @Test
    public void testSetCity() {
        System.out.println("Test setCity method");
        Location instance = new Location();
        String res = "Las Vegas";
        instance.setCity(res);
        assertTrue(instance.getCity().contains(res));
    }

    @Test
    public void testGetCountry() {
        System.out.println("Test getCountry method");
        Location instance = new Location();
        String res = "France";
        instance.setCountry(res);
        assertTrue(instance.getCountry().contains(res));
    }

    @Test
    public void testSetCountry() {
        System.out.println("Test setCountry method");
        String res = "France";
        Location instance = new Location();
        instance.setCountry(res);
        assertTrue(instance.getCountry().contains(res));
    }

    @Test
    public void testGetRegion() {
        System.out.println("Test getRegion method");
        Location instance = new Location();
        String res = "CBA";
        instance.setRegion(res);
        assertTrue(instance.getRegion().contains(res));
    }

    @Test
    public void testSetRegion() {
        System.out.println("Test setRegion method");
        String res = "CBA";
        Location instance = new Location();
        instance.setRegion(res);
        assertTrue(instance.getRegion().contains(res));
    }

    @Test
    public void testGetLat() {
        System.out.println("Test getLat method");
        Location instance = new Location();
        String res = "66";
        instance.setLat(res);
        assertTrue(instance.getLat().contains(res));
    }

    @Test
    public void testSetLat() {
        System.out.println("Test setLat method");
        String res = "66";
        Location instance = new Location();
        instance.setLat(res);
        assertTrue(instance.getLat().contains(res));
    }

    @Test
    public void testGetLon() {
        System.out.println("Test getLon method");
        Location instance = new Location();
        String res = "33";
        instance.setLon(res);
        assertTrue(instance.getLon().contains(res));
    }

    @Test
    public void testSetLon() {
        System.out.println("Test setLon method");
        String res = "33";
        Location instance = new Location();
        instance.setLon(res);
        assertTrue(instance.getLon().contains(res));
    }
    
    
    
}
