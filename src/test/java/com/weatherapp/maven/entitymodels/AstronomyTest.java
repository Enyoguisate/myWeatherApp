
package com.weatherapp.maven.entitymodels;
import org.junit.Test;
import static org.junit.Assert.*;


public class AstronomyTest {
    
    public AstronomyTest() {
    }    
    //Astronomy class test
    @Test
    public void testGetId() {
        System.out.println("Test getId method");
        Astronomy instance = new Astronomy();
        instance.setId(5);
        assertEquals(5, instance.getId());
    }

    @Test
    public void testSetId() {
        System.out.println("Test setId mmethod");
        Astronomy instance = new Astronomy();
        instance.setId(5);
        assertEquals(5,instance.getId());        
    }

    @Test
    public void testGetSunrise() {
        System.out.println("Test getSunrise method");
        Astronomy instance = new Astronomy();
        instance.setSunrise("07:48");  
        String res = "7:48";
        assertTrue(instance.getSunrise().contains(res));        
    }

    @Test
    public void testSetSunrise() {
        System.out.println("Test setSunrise method");
        Astronomy instance = new Astronomy();
        instance.setSunrise("07:48");
        String res = "07:48";
        assertTrue(instance.getSunrise().contains(res));
    }
    
    @Test
    public void testGetSunset() {
        System.out.println("Test getSunset method");
        Astronomy instance = new Astronomy();
        instance.setSunset("18:50");
        String res = "18:50";
        assertTrue(instance.getSunset().contains(res));        
    }

    @Test
    public void testSetSunset() {
        System.out.println("Test setSunset method");
        Astronomy instance = new Astronomy();
        instance.setSunset("18:50");
        String res = "18:50";
        assertTrue(instance.getSunset().contains(res));
    }

    
    
}
