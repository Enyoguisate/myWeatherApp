
package com.weatherapp.maven.entitymodels;
import org.junit.Test;
import static org.junit.Assert.*;


public class DayTest {
    
    public DayTest() {
    }
    //Test of class Day.
     
    @Test
    public void testGetId() {
        System.out.println("Test getId method");
        Day instance = new Day();
        instance.setId(1);
        assertEquals(1, instance.getId());
    }

    @Test
    public void testSetId() {
        System.out.println("Test setId method");
        Day instance = new Day();
        instance.setId(1);
        assertEquals(1,instance.getId());
    }

    @Test
    public void testGetDate() {
        System.out.println("Test getDate method");
        Day instance = new Day();
        Date expResult = null;
        Date result = instance.getDate();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetDate() {
        System.out.println("Test setDate method");
        Date date = new Date();
        Day instance = new Day();
        date.setId(1);
        date.setDate("03 May 2017");
        date.setDay("Mon");
        instance.setDate(date);
        assertEquals(date, instance.getDate());
    }

    @Test
    public void testGetTemp() {
        System.out.println("Test getTemp method");
        Day instance = new Day();
        Temperature temp = new Temperature();
        temp.setId(1);
        temp.setTemp("66");
        temp.setHigh("69");
        temp.setLow("61");
        instance.setTemp(temp);
        assertEquals(temp, instance.getTemp());
    }

    @Test
    public void testSetTemp() {
        System.out.println("Test setTemp method");
        Temperature temp = new Temperature();
        Day instance = new Day();
        temp.setId(1);
        temp.setTemp("66");
        temp.setHigh("69");
        temp.setLow("61");
        instance.setTemp(temp);
        assertEquals(temp, instance.getTemp());
    }

    @Test
    public void testGetConditionDescription() {
        System.out.println("Test getConditionDescription method");
        Day instance = new Day();
        String description = "Scattered Storms";
        instance.setConditionDescription(description);
        assertTrue(instance.getConditionDescription().contains(description));
    }

    @Test
    public void testSetConditionDescription() {
        System.out.println("Test setConditionDescription method");
        Day instance = new Day();
        String description = "Scattered Storms";
        instance.setConditionDescription(description);
        assertTrue(instance.getConditionDescription().contains(description));
    }

    @Test
    public void testGetWind() {
        System.out.println("Test getWind method");
        Day instance = new Day();
        Wind wind = new Wind();
        wind.setId(1);
        wind.setSpeed("14");
        wind.setDirection("N");
        instance.setWind(wind);
        assertEquals(wind, instance.getWind());
    }

    @Test
    public void testSetWind() {
        System.out.println("Test setWind method");
        Day instance = new Day();
        Wind wind = new Wind();
        wind.setId(1);
        wind.setSpeed("14");
        wind.setDirection("N");
        instance.setWind(wind);
        assertEquals(wind, instance.getWind());
    }

    @Test
    public void testGetAtmosphere() {
        System.out.println("Test getAtmosphere method");
        Day instance = new Day();
        Atmosphere atmos = new Atmosphere();
        atmos.setHumidity("66");
        atmos.setId(1);
        atmos.setPressure("77");
        atmos.setVisibility("14");
        instance.setAtmosphere(atmos);
        assertEquals(atmos, instance.getAtmosphere());
    }

    @Test
    public void testSetAtmosphere() {
        System.out.println("Test setAtmosphere method");
        Day instance = new Day();
        Atmosphere atmos = new Atmosphere();
        atmos.setHumidity("66");
        atmos.setId(1);
        atmos.setPressure("77");
        atmos.setVisibility("14");
        instance.setAtmosphere(atmos);
        assertEquals(atmos, instance.getAtmosphere());
    }

    @Test
    public void testGetAstronomy() {
        System.out.println("Test getAstronomy method");
        Day instance = new Day();
        Astronomy astro = new Astronomy();
        astro.setId(1);
        astro.setSunrise("07:43");
        astro.setSunset("18:55");
        instance.setAstronomy(astro);
        assertEquals(astro, instance.getAstronomy());
    }

    @Test
    public void testSetAstronomy() {
        System.out.println("Test setAstronomy method");
        Day instance = new Day();
        Astronomy astro = new Astronomy();
        astro.setId(1);
        astro.setSunrise("07:43");
        astro.setSunset("18:55");
        instance.setAstronomy(astro);
        assertEquals(astro, instance.getAstronomy());
    }

    @Test
    public void testGetLocation() {
        System.out.println("Test getLocation method");
        Day instance = new Day();
        Location loc = new Location();
        loc.setId(1);
        loc.setCity("Montgomery");
        loc.setCountry("EEUU");
        loc.setRegion("ALA");
        loc.setLat("66");
        loc.setLon("66");
        instance.setLocation(loc);
        assertEquals(loc, instance.getLocation());
    }

    @Test
    public void testSetLocation() {
        System.out.println("Test setLocation method");
        Day instance = new Day();
        Location loc = new Location();
        loc.setId(1);
        loc.setCity("Montgomery");
        loc.setCountry("EEUU");
        loc.setRegion("ALA");
        loc.setLat("66");
        loc.setLon("66");
        instance.setLocation(loc);
        assertEquals(loc, instance.getLocation());
    }

    
    
}
