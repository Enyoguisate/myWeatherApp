
package com.weatherapp.maven.entitymodels;
import org.junit.Test;
import static org.junit.Assert.*;

public class AtmosphereTest {
    
    public AtmosphereTest() {
    }
    
    //Test of class Atmosphere 
     
    @Test
    public void testGetId() {
        System.out.println("Test getId method");
        Atmosphere instance = new Atmosphere();
        instance.setId(4);        
        assertEquals(4, instance.getId());        
    }

    @Test
    public void testSetId() {
        System.out.println("Test setId method");
        Atmosphere instance = new Atmosphere();
        instance.setId(4);
        assertEquals(4,instance.getId());
    }

    @Test
    public void testGetHumidity() {
        System.out.println("Test getHumidity method");
        Atmosphere instance = new Atmosphere();
        String res = "66";
        instance.setHumidity(res);        
        assertTrue(instance.getHumidity().contains(res));        
    }

    @Test
    public void testSetHumidity() {
        System.out.println("Test setHumidity method");
        Atmosphere instance = new Atmosphere();
        String res = "66";
        instance.setHumidity(res);        
        assertTrue(instance.getHumidity().contains(res));
    }

    @Test
    public void testGetPressure() {
        System.out.println("Test getPressure method");
        Atmosphere instance = new Atmosphere();
        String res = "88";
        instance.setPressure(res);        
        assertTrue(instance.getPressure().contains(res));        
    }

    @Test
    public void testSetPressure() {
        System.out.println("Test setPressure method");
        Atmosphere instance = new Atmosphere();
        String res = "88";
        instance.setPressure(res);
        assertTrue(instance.getPressure().contains(res));
    }

    @Test
    public void testGetVisibility() {
        System.out.println("Test getVisibility method");
        Atmosphere instance = new Atmosphere();
        String res = "15";
        instance.setVisibility(res);
        assertTrue(instance.getVisibility().contains(res));        
    }

    @Test
    public void testSetVisibility() {
        System.out.println("Test setVisibility method");
        Atmosphere instance = new Atmosphere();
        String res = "15";
        instance.setVisibility(res);
        assertTrue(instance.getVisibility().contains(res));
    }

    
}
