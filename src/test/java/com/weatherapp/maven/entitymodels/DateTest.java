
package com.weatherapp.maven.entitymodels;
import org.junit.Test;
import static org.junit.Assert.*;


public class DateTest {
    
    public DateTest() {
    }
    
    //Test of class Date.
    
    @Test
    public void testGetId() {
        System.out.println("Test getId method");
        Date instance = new Date();
        instance.setId(3);
        assertEquals(3, instance.getId());        
    }

    @Test
    public void testSetId() {
        System.out.println("Test setId method");
        Date instance = new Date();
        instance.setId(3);
        assertEquals(3,instance.getId());
    }

    @Test
    public void testGetDate() {
        System.out.println("Test getDate method");
        Date instance = new Date();
        String res = "03 mar 2017";
        instance.setDate(res);
        assertTrue(instance.getDate().contains(res));
    }

    @Test
    public void testSetDate() {
        System.out.println("Test setDate method");
        Date instance = new Date();
        String res = "03 mar 2017";
        instance.setDate(res);
        assertTrue(instance.getDate().contains(res));
    }

    
    @Test
    public void testGetDay() {
        System.out.println("Test getDay method");
        Date instance = new Date();
        String res = "tue";
        instance.setDay(res);
        assertTrue(instance.getDay().contains(res));
    }

    
    @Test
    public void testSetDay() {
        System.out.println("Test setDay method");
        Date instance = new Date();
        String res = "tue";
        instance.setDay(res);
        assertTrue(instance.getDay().contains(res));
    }
    
}
