
package com.weatherapp.maven.controller;

import com.weatherapp.maven.adapter.YahooStringAdapter;
import com.weatherapp.maven.entitymodels.Day;
import org.easymock.EasyMock;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.http.HttpStatus;


public class WeatherAppControllerTest {
    
    public WeatherAppControllerTest() {
    }
    
    @Test
    public void testGetDay() {
        System.out.println("Test getDay method");           
        YahooStringAdapter yahooStringAdapterMock = EasyMock.createMock(YahooStringAdapter.class);        
        EasyMock.expect(yahooStringAdapterMock.getAllDays()).andReturn("someString");        
        EasyMock.replay(yahooStringAdapterMock);        
        WeatherAppController weatherAppController = new WeatherAppController(yahooStringAdapterMock);
        HttpStatus result = weatherAppController.getDay().getStatusCode();
        assertTrue(result.value()==200);        
        EasyMock.verify(yahooStringAdapterMock);
    }

    @Test
    public void testGetDay_int() {
        System.out.println("Test getDay method");
        YahooStringAdapter yahooStringAdapterMock = EasyMock.createMock(YahooStringAdapter.class);
        EasyMock.expect(yahooStringAdapterMock.getDayById(1)).andReturn("someString");
        EasyMock.replay(yahooStringAdapterMock);
        WeatherAppController weatherAppController = new WeatherAppController(yahooStringAdapterMock);
        HttpStatus result = weatherAppController.getDay(1).getStatusCode();
        assertTrue(result.value()==200);        
        EasyMock.verify(yahooStringAdapterMock);
    }

    @Test
    public void testPutDay() {
        System.out.println("Test putDay method");
        YahooStringAdapter yahooStringAdapterMock = EasyMock.createMock(YahooStringAdapter.class);
        Day dayMock = EasyMock.createMock(Day.class);
        EasyMock.expect(yahooStringAdapterMock.updateDay(dayMock)).andReturn("someString");
        EasyMock.replay(yahooStringAdapterMock,dayMock);
        WeatherAppController weatherAppController = new WeatherAppController(yahooStringAdapterMock);
        HttpStatus result = weatherAppController.putDay(dayMock).getStatusCode();
        assertTrue(result.value()==200);        
        EasyMock.verify(yahooStringAdapterMock,dayMock);
    }

    @Test
    public void testAddDay() {
        System.out.println("Test addDay method");
        YahooStringAdapter yahooStringAdapterMock = EasyMock.createMock(YahooStringAdapter.class);
        Day dayMock = EasyMock.createMock(Day.class);
        EasyMock.expect(yahooStringAdapterMock.addDay(dayMock)).andReturn("someString");
        EasyMock.replay(yahooStringAdapterMock,dayMock);
        WeatherAppController weatherAppController = new WeatherAppController(yahooStringAdapterMock);
        HttpStatus result = weatherAppController.addDay(dayMock).getStatusCode();
        assertTrue(result.value()==200);        
        EasyMock.verify(yahooStringAdapterMock,dayMock);
    }

    @Test
    public void testGetForecast() {
        System.out.println("Test getForecast method");
        YahooStringAdapter yahooStringAdapterMock = EasyMock.createMock(YahooStringAdapter.class);
        EasyMock.expect(yahooStringAdapterMock.getLocation(EasyMock.anyString(),EasyMock.anyString())).andReturn("someString");
        EasyMock.replay(yahooStringAdapterMock);
        WeatherAppController weatherAppController = new WeatherAppController(yahooStringAdapterMock);
        HttpStatus result = weatherAppController.getForecast("someCountry", "someCity").getStatusCode();
        assertTrue(result.value()==200);        
        EasyMock.verify(yahooStringAdapterMock);
    }
    
}
