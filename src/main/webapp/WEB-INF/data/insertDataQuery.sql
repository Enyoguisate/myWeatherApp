

INSERT INTO `astronomy` (`id_astronomy`, `sunrise`, `sunset`) VALUES
(1, '7:48', '18:39'),
(2, '7:48', '18:39'),
(3, '7:48', '18:39'),
(4, '7:48', '18:39'),
(5, '7:48', '18:39'),
(6, '7:48', '18:39'),
(7, '7:48', '18:39'),
(8, '7:48', '18:39'),
(9, '7:48', '18:39'),
(10, '7:48', '18:39');

-- --------------------------------------------------------


INSERT INTO `atmosphere` (`id_atmosphere`, `humidity`, `pressure`, `visibility`) VALUES
(1, '77', '961.0', '16.1'),
(2, '86', '971.0', '14.4'),
(3, '82', '958.0', '12.5'),
(4, '87', '963.0', '18.8'),
(5, '84', '971.0', '13.2'),
(6, '94', '981.0', '14.7'),
(7, '91', '941.0', '19.4'),
(8, '89', '965.0', '16.1'),
(9, '84', '957.0', '13.6'),
(10, '92', '968.0', '12.7');



-- --------------------------------------------------------


INSERT INTO `date` (`id_date`, `date`, `day`) VALUES
(1, '02 May 2017', 'Tue'),
(2, '03 May 2017', 'Wed'),
(3, '04 May 2017', 'Thu'),
(4, '05 May 2017', 'Fri'),
(5, '06 May 2017', 'Sat'),
(6, '07 May 2017', 'Sun'),
(7, '08 May 2017', 'Mon'),
(8, '09 May 2017', 'Tue'),
(9, '10 May 2017', 'Wed'),
(10, '11 May 2017', 'Thu');

-- --------------------------------------------------------



INSERT INTO `day` (`id_day`, `conditiondescription`) VALUES
(1, 'Mostly Cloudy'),
(2, 'Partly Cloudy'),
(3, 'Partly Cloudy'),
(4, 'Partly Cloudy'),
(5, 'Scattered Thunderstorms'),
(6, 'Thunderstorms'),
(7, 'Mostly Cloudy'),
(8, 'Partly Cloudy'),
(9, 'Scattered Thunderstorms'),
(10, 'Scattered Thunderstorms');

-- --------------------------------------------------------





INSERT INTO `location` (`id_location`, `city`, `country`, `region`, `lat`, `lon`) VALUES
(1, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(2, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(3, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(4, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(5, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(6, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(7, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(8, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(9, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),
(10, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929');



INSERT INTO `temperature` (`id_temperature`, `temp`, `high`, `low`) VALUES
(1, '58', '74', '54'),
(2, '65', '70', '60'),
(3, '64', '74', '56'),
(4, '64', '76', '61'),
(5, '67', '73', '64'),
(6, '69', '72', '62'),
(7, '61', '64', '57'),
(8, '58', '64', '54'),
(9, '61', '63', '60'),
(10, '58', '62', '52');



INSERT INTO `wind` (`id_wind`, `direction`, `speed`) VALUES
(1, 'O', '11'),
(2, 'NO', '15'),
(3, 'N', '8'),
(4, 'O', '13'),
(5, 'SO', '9'),
(6, 'O', '17'),
(7, 'N', '17'),
(8, 'NO', '15'),
(9, 'O', '14'),
(10, 'NO', '17');


