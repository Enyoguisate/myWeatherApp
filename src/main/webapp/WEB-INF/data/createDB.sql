 CREATE DATABASE IF NOT EXISTS WEATHER;

CREATE TABLE IF NOT EXISTS `astronomy` (
  `id_astronomy` int(11) NOT NULL,
  `sunrise` varchar(100) NOT NULL,
  `sunset` varchar(100) NOT NULL
) ;

CREATE TABLE IF NOT EXISTS `atmosphere` (
  `id_atmosphere` int(11) NOT NULL,
  `humidity` varchar(100) NOT NULL,
  `pressure` varchar(100) NOT NULL,
  `visibility` varchar(100) NOT NULL
) ;

CREATE TABLE IF NOT EXISTS `date` (
  `id_date` int(11) NOT NULL,
  `date` varchar(100) NOT NULL,
  `day` varchar(100) NOT NULL
) ;

CREATE TABLE IF NOT EXISTS `day` (
  `id_day` int(11) NOT NULL,
  `conditiondescription` varchar(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS `location` (
  `id_location` int(11) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `region` varchar(100) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lon` varchar(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS `temperature` (
  `id_temperature` int(11) NOT NULL,
  `temp` varchar(100) NOT NULL,
  `high` varchar(100) NOT NULL,
  `low` varchar(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS `wind` (
  `id_wind` int(11) NOT NULL,
  `direction` varchar(100) NOT NULL,
  `speed` varchar(100) NOT NULL
);














