
package com.weatherapp.maven.service;

import com.weatherapp.maven.dao.DaoAstronomyImpl;
import com.weatherapp.maven.dao.DaoAtmosphereImpl;
import com.weatherapp.maven.dao.DaoDateImpl;
import com.weatherapp.maven.dao.DaoDayImpl;
import com.weatherapp.maven.dao.DaoLocationImpl;
import com.weatherapp.maven.dao.DaoTemperatureImpl;
import com.weatherapp.maven.dao.DaoWindImpl;
import com.weatherapp.maven.entitymodels.Astronomy;
import com.weatherapp.maven.entitymodels.Atmosphere;
import com.weatherapp.maven.entitymodels.Date;
import com.weatherapp.maven.entitymodels.Day;
import com.weatherapp.maven.entitymodels.Location;
import com.weatherapp.maven.entitymodels.Temperature;
import com.weatherapp.maven.entitymodels.Wind;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WeatherService {
    
    @Autowired
    private DaoAstronomyImpl daoAstro;
    @Autowired
    private DaoAtmosphereImpl daoAtmo;
    @Autowired
    private DaoDateImpl daoDate;
    @Autowired
    private DaoDayImpl daoDay;
    @Autowired
    private DaoLocationImpl daoLoca;
    @Autowired
    private DaoTemperatureImpl daoTemp;
    @Autowired
    private DaoWindImpl daoWind;

    public WeatherService() {
    }
    
    
    public List<Day> getAllDay() {
        List<Astronomy> listAstro = new ArrayList<>();
        List<Atmosphere> listAtmo = new ArrayList<>();
        List<Date> listDate = new ArrayList<>();
        List<Location> listLoca = new ArrayList<>();
        List<Temperature> listTemp = new ArrayList<>();
        List<Wind> listWind = new ArrayList<>();
        List<Day> listDay = new ArrayList<>();
        try {
            listAstro = daoAstro.getAll();
            listAtmo = daoAtmo.getAll();
            listDate = daoDate.getAll();
            listLoca = daoLoca.getAll();
            listTemp = daoTemp.getAll();
            listWind = daoWind.getAll();
            listDay = daoDay.getAll();
        } catch (Exception ex) {
            System.out.println("Error: " + ex);
        }

        for (int i = 0; i < listDay.size(); i++) {
            listDay.get(i).setAstronomy(listAstro.get(i));
            listDay.get(i).setAtmosphere(listAtmo.get(i));
            listDay.get(i).setDate(listDate.get(i));
            listDay.get(i).setLocation(listLoca.get(i));
            listDay.get(i).setTemp(listTemp.get(i));
            listDay.get(i).setWind(listWind.get(i));
        }
        return listDay;
    }

    public void updateDay(Day day) {
        try {
            daoAstro.update(day.getAstronomy());
            daoAtmo.update(day.getAtmosphere());
            daoDate.update(day.getDate());
            daoDay.update(day);
            daoLoca.update(day.getLocation());
            daoTemp.update(day.getTemp());
            daoWind.update(day.getWind());
        } catch (Exception ex) {
            System.out.println("Error: " + ex);
        }
    }

    public void insertDay(Day day) {
        try {
            daoAstro.insert(day.getAstronomy());
            daoAtmo.insert(day.getAtmosphere());
            daoDate.insert(day.getDate());
            daoLoca.insert(day.getLocation());
            daoTemp.insert(day.getTemp());
            daoWind.insert(day.getWind());
            daoDay.insert(day);
        } catch (Exception ex) {
            System.out.println("Error: " + ex);
        }
    }
}
