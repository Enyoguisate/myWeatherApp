package com.weatherapp.maven.validations;

import com.weatherapp.maven.entitymodels.Day;
import com.weatherapp.maven.service.WeatherService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Validations {

    @Autowired
    public WeatherService weatherService;

    public boolean validateGetId(int id) {
        List<Day> daysList = weatherService.getAllDay();
        for (Day day : daysList) {
            if (day.getId() == id) {
                return true;
            }
        }
        return false;
    }

    public boolean validateForNull() {
        return weatherService.getAllDay() == null;
    }

    public boolean validateForNormalTempRange(List<Day> dayList) {
        List<Day> daysList = dayList;
        for (Day day : daysList) {
            if (Float.parseFloat(day.getTemp().getTemp()) < -80 || Float.parseFloat(day.getTemp().getTemp()) > 100) {
                return false;
            }
        }
        return true;
    }

}
