package com.weatherapp.maven.controller;

import com.weatherapp.maven.adapter.YahooStringAdapter;
import com.weatherapp.maven.dao.DbInit;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.weatherapp.maven.entitymodels.Day;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.ws.rs.core.MediaType;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class WeatherAppController {

    @Autowired
    private YahooStringAdapter yahoooStringAdapter;
    @Autowired
    private DbInit dbInit;

    public WeatherAppController() {
        
    }
    
    public WeatherAppController(YahooStringAdapter yahooStringAdapter){
        this.yahoooStringAdapter = yahooStringAdapter;
    }
   

    @RequestMapping(value = "/day",
            method = RequestMethod.GET)
    public ResponseEntity<String> getDay() {        
        checkInitialization();
        String response = yahoooStringAdapter.getAllDays();
        if (response.length() == 0) {
            return new ResponseEntity<>(":( Something went wrong, try again later", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/day/{id}",
            method = RequestMethod.GET)
    public ResponseEntity<String> getDay(@PathVariable("id") int id) {
        checkInitialization();
        String response = yahoooStringAdapter.getDayById(id);
        if (response.length() == 0){
            return new ResponseEntity<>(":( Something went wrong, try again later", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/updateday",
            method = RequestMethod.PUT)
    public ResponseEntity<String> putDay(@RequestBody Day day) {
        checkInitialization();
        String res = yahoooStringAdapter.updateDay(day);
        if (res.length() == 0) {
            return new ResponseEntity<>(":( Something went wrong, try again later", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/addday",
            method = RequestMethod.POST)
    public ResponseEntity<String> addDay(@RequestBody Day day) {
        checkInitialization();
        String res = yahoooStringAdapter.addDay(day);
        if (res.length() == 0) {
            return new ResponseEntity<>(":( Something went wrong, try again later", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/location/country/{country}/city/{city}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<String> getForecast(@PathVariable("country") String country, @PathVariable("city") String city) {
        String res = yahoooStringAdapter.getLocation(country, city);
        if (res.length() == 0) {
            return new ResponseEntity<>(":( Something went wrong, try again later", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }
    
    public void checkInitialization(){
        try {
            dbInit.initialize();
        } catch (Exception ex) {
            Logger.getLogger(WeatherAppController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
