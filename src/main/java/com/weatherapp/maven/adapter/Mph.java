package com.weatherapp.maven.adapter;

public interface Mph {

    double getMphSpeed(double speed);
}
