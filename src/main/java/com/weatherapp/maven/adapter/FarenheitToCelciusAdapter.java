
package com.weatherapp.maven.adapter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FarenheitToCelciusAdapter implements Celcius{
    @Autowired
    private FarenheitImpl farenheitTemp;
    
    public void setFarenheitTemp(FarenheitImpl farenheitTemp){
        this.farenheitTemp = farenheitTemp;
    }
    
    @Override
    public double getCelciusTemperature(double farenhitTemp) {
        return farenheitTemp.getFarenheitTemperature(farenhitTemp)-32;
    }
    
}
