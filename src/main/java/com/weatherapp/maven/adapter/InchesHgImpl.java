package com.weatherapp.maven.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InchesHgImpl implements InchesHg {

    @Override
    public double getInchesHg(double pressure) {
        return pressure;
    }

}
