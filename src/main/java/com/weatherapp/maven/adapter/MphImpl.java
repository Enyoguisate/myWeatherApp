package com.weatherapp.maven.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MphImpl implements Mph {

    @Override
    public double getMphSpeed(double speed) {
        return speed;
    }

}
