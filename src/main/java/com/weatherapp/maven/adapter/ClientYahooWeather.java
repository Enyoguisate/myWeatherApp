package com.weatherapp.maven.adapter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/v1/public")
public interface ClientYahooWeather {

    @GET
    @Path("/yql")
    String getWeather(@QueryParam("q") String queryConsult, @QueryParam("format") String format, @QueryParam("env") String env);

}
