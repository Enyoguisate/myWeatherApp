package com.weatherapp.maven.adapter;

public interface Farenheit {

    double getFarenheitTemperature(double temp);
}
