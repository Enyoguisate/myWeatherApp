package com.weatherapp.maven.adapter;

public interface InchesHg {

    double getInchesHg(double pressure);
}
