package com.weatherapp.maven.adapter;

public interface Celcius {

    double getCelciusTemperature(double temp);
}
