package com.weatherapp.maven.adapter;

public interface YahooNewConsult {

    String getLocation(String country, String city);
}
