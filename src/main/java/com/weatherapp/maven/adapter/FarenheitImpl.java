package com.weatherapp.maven.adapter;

import org.springframework.stereotype.Component;

@Component
public class FarenheitImpl implements Farenheit {

    @Override
    public double getFarenheitTemperature(double farenheitTemp) {
        return farenheitTemp;
    }

}
