package com.weatherapp.maven.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MphToKphAdapter implements Kph {

    @Autowired
    private MphImpl mphSpeed;

    public void setMphImpl(MphImpl speed) {
        this.mphSpeed = speed;
    }

    @Override
    public double getKphSpeed(double speed) {
        return mphSpeed.getMphSpeed(speed) * (16 / 10);
    }

}
