package com.weatherapp.maven.adapter;

public interface Kilometer {

    double getKilometer(double distance);
}
