package com.weatherapp.maven.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InchesHgToMilimetersHgAdapter implements MilimetersHg {

    @Autowired
    private InchesHgImpl inchesHg;

    public void setInchesHgImpl(InchesHgImpl inchesHg) {
        this.inchesHg = inchesHg;
    }

    @Override
    public double getMilimetersHg(double pressure) {
        return inchesHg.getInchesHg(pressure) * (254 / 10);
    }

}
