package com.weatherapp.maven.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MileToKilometerAdapter implements Kilometer {

    @Autowired
    private MileImpl mileDistance;

    public void setMileDistance(MileImpl mileImpl) {
        this.mileDistance = mileImpl;
    }

    @Override
    public double getKilometer(double distance) {

        return mileDistance.getMileDistance(distance) * (16 / 10);
    }

}
