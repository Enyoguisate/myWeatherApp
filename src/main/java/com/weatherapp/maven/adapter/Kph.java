package com.weatherapp.maven.adapter;

public interface Kph {

    double getKphSpeed(double speed);
}
