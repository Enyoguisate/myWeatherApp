package com.weatherapp.maven.adapter;

public interface MilimetersHg {

    double getMilimetersHg(double pressure);
}
