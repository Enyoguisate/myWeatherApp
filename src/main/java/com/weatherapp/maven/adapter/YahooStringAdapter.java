package com.weatherapp.maven.adapter;

import com.weatherapp.maven.entitymodels.Day;
import com.weatherapp.maven.proxy.ProxyWeather;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class YahooStringAdapter implements YahooNewConsult {

    @Autowired
    private ProxyWeather proxyWeather;

    @Override
    public String getLocation(String country, String city) {
        String response = proxyWeather.getWeatherResponse(country, city);
        return response;
    }
    
    public void setTestProxyWeather(ProxyWeather proxyWeather){
        this.proxyWeather = proxyWeather;
    }

    public String getAllDays() {
        List<Day> dayList;
        String res = "";
        dayList = proxyWeather.getDaysList();
        if (dayList != null) {
            for (int i = 0; i < dayList.size(); i++) {
                res += dayList.get(i).toString();
            }
            return res;
        }
        return "";
    }

    public String getDayById(int id) {
        List<Day> dayList = proxyWeather.getDaysList();
        if (dayList != null) {
            for (int i = 0; i < dayList.size(); i++) {
                if (dayList.get(i).getId() == id) {
                    return dayList.get(i).toString();
                }
            }
        }
        return "";
    }

    public String updateDay(Day day) {
        String res = "";
        List<Day> dayList = proxyWeather.updateDay(day);
        if (dayList != null) {
            for (Day d : dayList) {
                res += d.toString();
            }
            return res;
        }
        return "";
    }

    public String addDay(Day day) {
        String res = "";
        List<Day> dayList = proxyWeather.addDay(day);
        if (dayList != null) {
            for (Day d : dayList) {
                res += d.toString();
            }
            return res;
        }
        return "";

    }

}
