package com.weatherapp.maven.adapter;

public interface Mile {

    double getMileDistance(double distance);
}
