package com.weatherapp.maven.dao;

import com.weatherapp.maven.entitymodels.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoDateImpl implements DaoForecast<Date> {
    
    @Autowired
    private DaoExecuteStatements daoExecuteStatements;

    @Override
    public void insert(Date date) {
        try {
            int id = daoExecuteStatements.getMaxId();
            String query = "insert into date (id_date, date, day) "
                    + "values(" + id + ",'" 
                    + date.getDate() + "','"
                    + date.getDay() + "')";
            daoExecuteStatements.executeInsertStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoDateImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Date date) {
        try {
            String query = "update date set date = '"
                    + date.getDate() + "', day = '" 
                    + date.getDay() + "' where id_date = " 
                    + date.getId();
            daoExecuteStatements.executeUpdateStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoDateImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Date date) {
        try {
            String query = "delete from date where id_date = " + date.getId() + ");";
            daoExecuteStatements.executeDeleteStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoDateImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Date> getAll() {
        List<Date> aList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = daoExecuteStatements.getConnection().prepareStatement(
                    "select * from date");
            aList = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Date date = new Date();
                date.setId(resultSet.getInt("id_date"));
                date.setDate(resultSet.getString("date"));
                date.setDay(resultSet.getString("day"));
                aList.add(date);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoDateImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            daoExecuteStatements.closeConnection();
        }
        return aList;

    }

}
