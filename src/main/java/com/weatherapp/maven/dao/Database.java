package com.weatherapp.maven.dao;

import java.sql.*;

public class Database {

    private static Connection connection;
    private static String jdbcDriver;
    private static String url;
    private static String username;
    private static String password;

    public Database(String jdbcDriver, String url, String username, String password) {
        connection = null;
        this.jdbcDriver = jdbcDriver;
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public Database() {
    }

    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                Class.forName(jdbcDriver);
                connection = DriverManager.getConnection(url, username, password);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        return connection;
    }

    
    
    public void setJdbcDriver(String jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }

    public static String getJdbcDriver() {
        return jdbcDriver;
    }

    public static String getUrl() {
        return url;
    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void closeConnection() throws Exception {
        if (connection != null || !connection.isClosed()) {
            connection.close();
            connection = null;
        }
    }

}
