package com.weatherapp.maven.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DbInit {
    
    @Autowired
    private Database connection;

    public DbInit() {
        //this.connection = new Database(); 
        
    }

    public Connection getConnection() {
        return connection.getConnection();
    }

    public void initialize() throws Exception {
        try {
            if (!verifyExistence()) {
                if (!checkforData()) {
                    loadDb();
                }
            } else {
                createDb();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbInit.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    public void loadDb() throws Exception {
        try {
            for (String str : insertQuery()) {
                PreparedStatement preparedStatement = getConnection().prepareStatement(str);
                preparedStatement.executeQuery();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbInit.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connection.closeConnection();
        }
    }

    public void createDb() throws Exception {
        try {
            for (String str : createDbQuery()) {
                PreparedStatement preparedStatement = getConnection().prepareStatement(str);
                preparedStatement.executeQuery();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbInit.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connection.closeConnection();
        }
    }

    public boolean checkforData() throws Exception {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("select * from day");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet == null) {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbInit.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connection.closeConnection();
        }
        return true;
    }

    public boolean verifyExistence() {
        File f = new File("~/h2weatherdb");
        return f.exists();
    }

    public String[] insertQuery() {
        String[] stringQ = new String[7];
        stringQ[0] = "INSERT INTO `astronomy` (`id_astronomy`, `sunrise`, `sunset`) VALUES\n"
                + "(1, '7:48', '18:39'),\n"
                + "(2, '7:48', '18:39'),\n"
                + "(3, '7:48', '18:39'),\n"
                + "(4, '7:48', '18:39'),\n"
                + "(5, '7:48', '18:39'),\n"
                + "(6, '7:48', '18:39'),\n"
                + "(7, '7:48', '18:39'),\n"
                + "(8, '7:48', '18:39'),\n"
                + "(9, '7:48', '18:39'),\n"
                + "(10, '7:48', '18:39');";
        stringQ[1] = "INSERT INTO `atmosphere` (`id_atmosphere`, `humidity`, `pressure`, `visibility`) VALUES\n"
                + "(1, '77', '961.0', '16.1'),\n"
                + "(2, '86', '971.0', '14.4'),\n"
                + "(3, '82', '958.0', '12.5'),\n"
                + "(4, '87', '963.0', '18.8'),\n"
                + "(5, '84', '971.0', '13.2'),\n"
                + "(6, '94', '981.0', '14.7'),\n"
                + "(7, '91', '941.0', '19.4'),\n"
                + "(8, '89', '965.0', '16.1'),\n"
                + "(9, '84', '957.0', '13.6'),\n"
                + "(10, '92', '968.0', '12.7');";
        stringQ[2] = "INSERT INTO `date` (`id_date`, `date`, `day`) VALUES\n"
                + "(1, '02 May 2017', 'Tue'),\n"
                + "(2, '03 May 2017', 'Wed'),\n"
                + "(3, '04 May 2017', 'Thu'),\n"
                + "(4, '05 May 2017', 'Fri'),\n"
                + "(5, '06 May 2017', 'Sat'),\n"
                + "(6, '07 May 2017', 'Sun'),\n"
                + "(7, '08 May 2017', 'Mon'),\n"
                + "(8, '09 May 2017', 'Tue'),\n"
                + "(9, '10 May 2017', 'Wed'),\n"
                + "(10, '11 May 2017', 'Thu');";
        stringQ[3] = "INSERT INTO `day` (`id_day`, `conditiondescription`) VALUES\n"
                + "(1, 'Mostly Cloudy'),\n"
                + "(2, 'Partly Cloudy'),\n"
                + "(3, 'Partly Cloudy'),\n"
                + "(4, 'Partly Cloudy'),\n"
                + "(5, 'Scattered Thunderstorms'),\n"
                + "(6, 'Thunderstorms'),\n"
                + "(7, 'Mostly Cloudy'),\n"
                + "(8, 'Partly Cloudy'),\n"
                + "(9, 'Scattered Thunderstorms'),\n"
                + "(10, 'Scattered Thunderstorms');";
        stringQ[4] = "INSERT INTO `location` (`id_location`, `city`, `country`, `region`, `lat`, `lon`) VALUES\n"
                + "(1, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(2, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(3, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(4, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(5, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(6, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(7, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(8, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(9, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929'),\n"
                + "(10, 'Cordoba', 'Argentina', 'CBA', '-31.403959', '-64.197929');";
        stringQ[5] = "INSERT INTO `temperature` (`id_temperature`, `temp`, `high`, `low`) VALUES\n"
                + "(1, '58', '74', '54'),\n"
                + "(2, '65', '70', '60'),\n"
                + "(3, '64', '74', '56'),\n"
                + "(4, '64', '76', '61'),\n"
                + "(5, '67', '73', '64'),\n"
                + "(6, '69', '72', '62'),\n"
                + "(7, '61', '64', '57'),\n"
                + "(8, '58', '64', '54'),\n"
                + "(9, '61', '63', '60'),\n"
                + "(10, '58', '62', '52');\n"
                + "";
        stringQ[6] = "INSERT INTO `wind` (`id_wind`, `direction`, `speed`) VALUES\n"
                + "(1, 'O', '11'),\n"
                + "(2, 'NO', '15'),\n"
                + "(3, 'N', '8'),\n"
                + "(4, 'O', '13'),\n"
                + "(5, 'SO', '9'),\n"
                + "(6, 'O', '17'),\n"
                + "(7, 'N', '17'),\n"
                + "(8, 'NO', '15'),\n"
                + "(9, 'O', '14'),\n"
                + "(10, 'NO', '17');";
        return stringQ;
    }

    public String[] createDbQuery() {
        String[] createQ = new String[8];
        createQ[0] = "CREATE DATABASE IF NOT EXISTS WEATHER;";
        createQ[1] = "CREATE TABLE IF NOT EXISTS `astronomy` (\n"
                + "  `id_astronomy` int(11) NOT NULL,\n"
                + "  `sunrise` varchar(100) NOT NULL,\n"
                + "  `sunset` varchar(100) NOT NULL\n"
                + ") ;";
        createQ[2] = "CREATE TABLE IF NOT EXISTS `atmosphere` (\n"
                + "  `id_atmosphere` int(11) NOT NULL,\n"
                + "  `humidity` varchar(100) NOT NULL,\n"
                + "  `pressure` varchar(100) NOT NULL,\n"
                + "  `visibility` varchar(100) NOT NULL\n"
                + ") ;";
        createQ[3] = "CREATE TABLE IF NOT EXISTS `date` (\n"
                + "  `id_date` int(11) NOT NULL,\n"
                + "  `date` varchar(100) NOT NULL,\n"
                + "  `day` varchar(100) NOT NULL\n"
                + ") ;";
        createQ[4] = "CREATE TABLE IF NOT EXISTS `day` (\n"
                + "  `id_day` int(11) NOT NULL,\n"
                + "  `conditiondescription` varchar(100) NOT NULL\n"
                + ");";
        createQ[5] = "CREATE TABLE IF NOT EXISTS `location` (\n"
                + "  `id_location` int(11) NOT NULL,\n"
                + "  `city` varchar(100) NOT NULL,\n"
                + "  `country` varchar(100) NOT NULL,\n"
                + "  `region` varchar(100) NOT NULL,\n"
                + "  `lat` varchar(100) NOT NULL,\n"
                + "  `lon` varchar(100) NOT NULL\n"
                + ");";
        createQ[6] = "CREATE TABLE IF NOT EXISTS `temperature` (\n"
                + "  `id_temperature` int(11) NOT NULL,\n"
                + "  `temp` varchar(100) NOT NULL,\n"
                + "  `high` varchar(100) NOT NULL,\n"
                + "  `low` varchar(100) NOT NULL\n"
                + ");";
        createQ[7] = "CREATE TABLE IF NOT EXISTS `wind` (\n"
                + "  `id_wind` int(11) NOT NULL,\n"
                + "  `direction` varchar(100) NOT NULL,\n"
                + "  `speed` varchar(100) NOT NULL\n"
                + ");";
        return createQ;
    }
}
