package com.weatherapp.maven.dao;

import com.weatherapp.maven.entitymodels.Atmosphere;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoAtmosphereImpl implements DaoForecast<Atmosphere> {

    @Autowired
    private DaoExecuteStatements daoExecuteStatements;

    @Override
    public void insert(Atmosphere atmo) {
        try {
            int id = daoExecuteStatements.getMaxId();
            String query = "insert into Atmosphere (id_atmosphere, humidity, "
                    + "pressure, visibility) values(" 
                    + id + ",'" 
                    + atmo.getHumidity() + "','" 
                    + atmo.getPressure() + "','" 
                    + atmo.getVisibility() + "')";
            daoExecuteStatements.executeInsertStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoAtmosphereImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Atmosphere atmo) {
        try {
            String query = "update atmosphere set humidity = '" 
                    + atmo.getPressure() + "', pressure = '" 
                    + atmo.getPressure() + "', visibility = '" 
                    + atmo.getVisibility() + "' where id_atmosphere = " 
                    + atmo.getId();
            daoExecuteStatements.executeUpdateStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoAtmosphereImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public void delete(Atmosphere atmo){
        try {
            String query = "delete from atmosphere where id_atmosphere = " 
                    + atmo.getId() + ");";
            daoExecuteStatements.executeDeleteStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoAtmosphereImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public List<Atmosphere> getAll() {
        List<Atmosphere> aList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = daoExecuteStatements.getConnection().prepareStatement(
                    "select * from atmosphere");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Atmosphere atmo = new Atmosphere();
                atmo.setId(resultSet.getInt("id_atmosphere"));
                atmo.setHumidity(resultSet.getString("humidity"));
                atmo.setPressure(resultSet.getString("pressure"));
                atmo.setVisibility(resultSet.getString("visibility"));
                aList.add(atmo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoAtmosphereImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            daoExecuteStatements.closeConnection();
        }
        return aList;

    }

    
}
