package com.weatherapp.maven.dao;

import com.weatherapp.maven.entitymodels.Astronomy;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoAstronomyImpl implements DaoForecast<Astronomy> {

    @Autowired
    private DaoExecuteStatements daoExecuteStatements;

    @Override
    public void insert(Astronomy astronomy) {
        try {
            int id = daoExecuteStatements.getMaxId();
            String query = "insert into Astronomy (id_astronomy, "
                    + "sunrise, sunset) values(" + id + ",'" 
                    + astronomy.getSunrise() + "','" 
                    + astronomy.getSunset() + "')";
            daoExecuteStatements.executeInsertStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoAstronomyImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Astronomy astronomy) {
        try {
            String query = "update astronomy set sunrise = '" 
                    + astronomy.getSunrise() + "', sunset = '" 
                    + astronomy.getSunset() + "' where id_astronomy = "
                    + astronomy.getId();
            daoExecuteStatements.executeUpdateStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoAstronomyImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Astronomy astronomy) {
        try {
            String query = "delete from astronomy where id_astronomy = "
                    + astronomy.getId() + ";";
            daoExecuteStatements.executeDeleteStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoAstronomyImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Astronomy> getAll() throws Exception {
        List<Astronomy> aList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = daoExecuteStatements.getConnection()
                    .prepareStatement("select * from astronomy");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Astronomy astro = new Astronomy();
                astro.setId(resultSet.getInt("id_astronomy"));
                astro.setSunrise(resultSet.getString("sunrise"));
                astro.setSunset(resultSet.getString("sunset"));
                aList.add(astro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoAstronomyImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            daoExecuteStatements.closeConnection();
        }
        return aList;
    }

}
