package com.weatherapp.maven.dao;

public interface DaoForecast<T> {

    public void insert(T obj) throws Exception;

    public void update(T obj) throws Exception;

    public void delete(T obj) throws Exception;

    public java.util.List<T> getAll() throws Exception;
}
