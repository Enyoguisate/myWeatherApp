package com.weatherapp.maven.dao;

import com.weatherapp.maven.entitymodels.Temperature;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoTemperatureImpl implements DaoForecast<Temperature> {

    @Autowired
    private DaoExecuteStatements daoExecuteStatements;

    @Override
    public void insert(Temperature temp) {
        try {
            int id = daoExecuteStatements.getMaxId();
            String query = "insert into temperature (id_temperature, temp, high, low) "
                    + "values("
                    + id + ",'"
                    + temp.getTemp() + "','"
                    + temp.getHigh() + "','"
                    + temp.getLow() + "')";
            daoExecuteStatements.executeInsertStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoTemperatureImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Temperature temp) {
        try {
            String query = "update temperature set temp = '"
                    + temp.getId() + "', high = '"
                    + temp.getHigh() + "', low = '"
                    + temp.getLow() + "' where id_temperature = "
                    + temp.getId();
            daoExecuteStatements.executeUpdateStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoTemperatureImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Temperature temp) {
        try {
            String query = "delete from temperature where id_temperature = "
                    + temp.getId() + ");";
            daoExecuteStatements.executeDeleteStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoTemperatureImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Temperature> getAll() {
        List<Temperature> aList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = daoExecuteStatements.getConnection().prepareStatement(
                    "select * from temperature");
            aList = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Temperature temp = new Temperature();
                temp.setId(resultSet.getInt("id_temperature"));
                temp.setTemp(resultSet.getString("temp"));
                temp.setHigh(resultSet.getString("high"));
                temp.setLow(resultSet.getString("low"));
                aList.add(temp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoTemperatureImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            daoExecuteStatements.closeConnection();
        }
        return aList;

    }

}
