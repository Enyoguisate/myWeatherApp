package com.weatherapp.maven.dao;

import com.weatherapp.maven.entitymodels.Wind;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoWindImpl implements DaoForecast<Wind> {

    @Autowired
    private DaoExecuteStatements daoExecuteStatements;

    @Override
    public void insert(Wind wind) throws Exception {        
        try {
            int id = daoExecuteStatements.getMaxId();
            String query = "insert into wind (id_wind, direction, speed) "
                    + "values(" 
                    + id + ",'" 
                    + wind.getDirection() + "','" 
                    + wind.getSpeed() + "')";
            daoExecuteStatements.executeInsertStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoWindImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public void update(Wind wind) throws Exception {
        try {
            String query = "update wind set direction = '"
                    + wind.getDirection() + "', speed = '" 
                    + wind.getSpeed() + "' where id_wind = " 
                    + wind.getId();            
            daoExecuteStatements.executeUpdateStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoWindImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public void delete(Wind wind) throws Exception {
        try {
            String query = "delete from wind where id_wind= " + wind.getId() + ";";
            daoExecuteStatements.executeDeleteStatement(query);            
        } catch (Exception ex) {
            Logger.getLogger(DaoWindImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public List<Wind> getAll()  {
        List<Wind> aList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = daoExecuteStatements.getConnection().prepareStatement(
                    "select * from wind");
            aList = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Wind wind = new Wind();
                wind.setId(resultSet.getInt("id_wind"));
                wind.setDirection(resultSet.getString("direction"));
                wind.setSpeed(resultSet.getString("speed"));
                aList.add(wind);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoWindImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            daoExecuteStatements.closeConnection();
        }
        return aList;

    }

    
}
