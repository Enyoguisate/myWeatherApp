package com.weatherapp.maven.dao;

import com.weatherapp.maven.entitymodels.Day;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoDayImpl implements DaoForecast<Day> {

    @Autowired
    private DaoExecuteStatements daoExecuteStatements;

    @Override
    public void insert(Day day) {
        try {
            int id = daoExecuteStatements.getMaxId();
            String query = "insert into day (id_day, conditiondescription) "
                    + "values(" + id + ",'" 
                    + day.getConditionDescription() + "')";
            daoExecuteStatements.executeInsertStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoDayImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Day day) {
        try {
            String query = "update day set conditiondescription = '"
                    + day.getConditionDescription() + "' where id_day = " 
                    + day.getId();
            daoExecuteStatements.executeUpdateStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoDayImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Day day) {
        try {
            String query = "delete from day where id_day = " + day.getId() + ");";
            daoExecuteStatements.executeDeleteStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoDayImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Day> getAll() {
        List<Day> aList = new ArrayList<>();
        try {
            aList = new ArrayList<>();
            PreparedStatement preparedStatement = daoExecuteStatements.getConnection().prepareStatement(
                    "select * from day");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Day day = new Day();
                day.setId(resultSet.getInt("id_day"));
                day.setConditionDescription(resultSet.getString("conditiondescription"));
                aList.add(day);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoDayImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            daoExecuteStatements.closeConnection();
        }
        return aList;

    }

}
