package com.weatherapp.maven.dao;

import com.weatherapp.maven.entitymodels.Location;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoLocationImpl implements DaoForecast<Location> {

    @Autowired
    private DaoExecuteStatements daoExecuteStatements;

    @Override
    public void insert(Location loca) {
        try {
            int id = daoExecuteStatements.getMaxId();
            String query = "insert into location (id_location, city, country, "
                    + "region, lat, lon) values(" + id + ",'"
                    + loca.getCity() + "','" + loca.getCountry() + "','"
                    + loca.getRegion() + "','" + loca.getLat() + "','"
                    + loca.getLon() + "')";
            daoExecuteStatements.executeInsertStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoLocationImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Location loca) {
        try {
            String query = "update location set city = '" 
                    + loca.getCity() + "', country = '" 
                    + loca.getCountry() + "', region = '" 
                    + loca.getRegion() + "', lat = '" 
                    + loca.getLat() + "', lon = '" 
                    + loca.getLon() + "' where id_location = " 
                    + loca.getId();
            daoExecuteStatements.executeUpdateStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoLocationImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Location loca) {
        try {
            String query = "delete from location where id_location = "
                    + loca.getId() + ");";
            daoExecuteStatements.executeDeleteStatement(query);
        } catch (Exception ex) {
            Logger.getLogger(DaoLocationImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Location> getAll() {
        List<Location> aList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = daoExecuteStatements.getConnection().prepareStatement(
                    "select * from location");
            aList = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Location loca = new Location();
                loca.setId(resultSet.getInt("id_location"));
                loca.setCity(resultSet.getString("city"));
                loca.setCountry(resultSet.getString("country"));
                loca.setRegion(resultSet.getString("region"));
                loca.setLat(resultSet.getString("lat"));
                loca.setLon(resultSet.getString("lon"));
                aList.add(loca);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoLocationImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            daoExecuteStatements.closeConnection();
        }
        return aList;

    }

}
