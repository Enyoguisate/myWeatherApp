package com.weatherapp.maven.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class DaoExecuteStatements {

    private Database connection;

    public DaoExecuteStatements() {
        this.connection = new Database();
    }

    public Connection getConnection() {
        return connection.getConnection();
    }
    
    public void closeConnection(){
        try {
            this.connection.closeConnection();
        } catch (Exception ex) {
            Logger.getLogger(DaoExecuteStatements.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void executeInsertStatement(String query) {
        try {            
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.executeUpdate();
        }catch(SQLException ex){
            Logger.getLogger(DaoExecuteStatements.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            try {
                connection.closeConnection();
            } catch (Exception ex) {
                Logger.getLogger(DaoExecuteStatements.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void executeUpdateStatement(String query){        
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoExecuteStatements.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.closeConnection();
            } catch (Exception ex) {
                Logger.getLogger(DaoExecuteStatements.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void executeDeleteStatement(String query){
        //setConnection();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoExecuteStatements.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.closeConnection();
            } catch (Exception ex) {
                Logger.getLogger(DaoExecuteStatements.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    

    public int getMaxId() throws Exception {
        //GetMaxId method, this method is use to get the max id from a table
        int res = 0;
        String sqlQuery = "Select max(id_astronomy) as MAXID from astronomy";
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(sqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                res = resultSet.getInt("MAXID") + 1;
            }
        } catch (Exception ex) {
            System.out.println("Error on Getting max id: " + ex);
        } finally {
            connection.closeConnection();
        }
        return res;
    }
}
