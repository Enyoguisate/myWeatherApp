package com.weatherapp.maven.transformer;

import com.fasterxml.jackson.databind.JsonNode;
import com.weatherapp.maven.builder.*;
import com.weatherapp.maven.entitymodels.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Component;

@Component
public class ForecastTransformer {

    public static List<Day> transformJsonNodeToDaysList(JsonNode jsonNode) {
        try {
            List<Astronomy> astroList = transformJsonNodeToAstronomyList(jsonNode);
            List<Atmosphere> atmoList = transformJsonNodeToAtmosphereList(jsonNode);
            List<Location> locaList = transformJsonNodeToLocationList(jsonNode);
            List<Wind> windList = transformJsonNodeToWindList(jsonNode);
            List<Date> dateList = transformJsonNodeToDateList(jsonNode);
            List<Temperature> tempList = transformJsonNodeToTemperatureList(jsonNode);
            List<Day> dayList = transformJsonNodeToDayList(jsonNode);
            for (int i = 0; i < dayList.size(); i++) {
                dayList.get(i).setAstronomy(astroList.get(i));
                dayList.get(i).setAtmosphere(atmoList.get(i));
                dayList.get(i).setLocation(locaList.get(i));
                dayList.get(i).setWind(windList.get(i));
                dayList.get(i).setDate(dateList.get(i));
                dayList.get(i).setTemp(tempList.get(i));                
            }
            return dayList;
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
//**************************************

    public static List<Astronomy> transformJsonNodeToAstronomyList(JsonNode jsonNode) {
        List<Astronomy> astroList = new ArrayList<>();
        try {
            JsonNode astronomyNode = jsonNode.get("astronomy");
            for (int i = 1; i < 11; i++) {
                AstronomyBuilder astroBuilder = new AstronomyBuilder();
                Astronomy astro = astroBuilder.id(i).sunset(astronomyNode.get("sunset").asText()).sunrise(astronomyNode.get("sunrise").asText()).build();
                astroList.add(astro);
            }
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return astroList;
    }

    public static List<Atmosphere> transformJsonNodeToAtmosphereList(JsonNode jsonNode) {
        List<Atmosphere> atmoList = new ArrayList<>();
        try {
            JsonNode atmosphereNode = jsonNode.get("atmosphere");
            for (int i = 1; i < 11; i++) {
                AtmosphereBuilder atmoBuilder = new AtmosphereBuilder();
                Atmosphere atmo = atmoBuilder.id(i).humidity(atmosphereNode.get("humidity").asText()).pressure(atmosphereNode.get("pressure").asText()).visibility(atmosphereNode.get("visibility").asText()).build();
                atmoList.add(atmo);
            }
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return atmoList;
    }

    public static List<Location> transformJsonNodeToLocationList(JsonNode jsonNode) {
        List<Location> locaList = new ArrayList<>();
        try {
            JsonNode itemNode = jsonNode.get("item");
            JsonNode locationNode = jsonNode.get("location");
            for (int i = 1; i < 11; i++) {
                LocationBuilder locaBuilder = new LocationBuilder();
                Location loca = locaBuilder.id(i).city(locationNode.get("city").asText()).country(locationNode.get("country").asText()).region(locationNode.get("region").asText()).lat(itemNode.get("lat").asText()).lon(itemNode.get("long").asText()).build();
                locaList.add(loca);
            }
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return locaList;
    }

    public static List<Wind> transformJsonNodeToWindList(JsonNode jsonNode) {
        List<Wind> windList = new ArrayList<>();
        try {
            JsonNode windNode = jsonNode.get("wind");
            for (int i = 1; i < 11; i++) {
                WindBuilder windBuilder = new WindBuilder();
                Wind wind = windBuilder.id(i).speed(windNode.get("speed").asText()).direction(windNode.get("direction").asText()).build();
                windList.add(wind);
            }
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return windList;
    }

    public static List<Date> transformJsonNodeToDateList(JsonNode jsonNode) {
        List<Date> dateList = new ArrayList<>();
        try {
            JsonNode itemNode = jsonNode.get("item");
            for (int i = 1; i < 11; i++) {
                DateBuilder dateBuilder = new DateBuilder();
                Date date = dateBuilder.id(i).date(itemNode.get("forecast").get(i - 1).get("date").asText()).day(itemNode.get("forecast").get(i - 1).get("day").asText()).build();
                dateList.add(date);
            }
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dateList;
    }

    public static List<Temperature> transformJsonNodeToTemperatureList(JsonNode jsonNode) {
        List<Temperature> tempList = new ArrayList<>();
        try {
            JsonNode itemNode = jsonNode.get("item");
            for (int i = 1; i < 11; i++) {
                TemperatureBuilder tempBuilder = new TemperatureBuilder();
                Temperature temp = tempBuilder.id(i).temp(itemNode.get("condition").get("temp").asText()).high(itemNode.get("forecast").get(i - 1).get("high").asText()).low(itemNode.get("forecast").get(i - 1).get("low").asText()).build();
                tempList.add(temp);
            }
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempList;
    }

    public static List<Day> transformJsonNodeToDayList(JsonNode jsonNode) {
        List<Day> dayList = new ArrayList<>();
        try {
            JsonNode itemNode = jsonNode.get("item");
            for (int i = 1; i < 11; i++) {
                DayBuilder dayBuilder = new DayBuilder();
                Day day = dayBuilder.id(i).conditionDescription(itemNode.get("forecast").get(i - 1).get("text").asText()).build();
                dayList.add(day);
            }
        } catch (Exception ex) {
            Logger.getLogger(ForecastTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dayList;
    }
}
