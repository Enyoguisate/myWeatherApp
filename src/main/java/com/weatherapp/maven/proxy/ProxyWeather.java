package com.weatherapp.maven.proxy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weatherapp.maven.validations.Validations;
import com.weatherapp.maven.adapter.FarenheitToCelciusAdapter;
import com.weatherapp.maven.adapter.InchesHgToMilimetersHgAdapter;
import com.weatherapp.maven.adapter.MileToKilometerAdapter;
import com.weatherapp.maven.adapter.MphToKphAdapter;
import com.weatherapp.maven.adapter.ClientYahooWeather;
import com.weatherapp.maven.entitymodels.Day;
import com.weatherapp.maven.service.WeatherService;
import com.weatherapp.maven.transformer.ForecastTransformer;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProxyWeather implements ClientYahooWeather{

    @Autowired
    private FarenheitToCelciusAdapter farenheitToCelciusAdapter;
    @Autowired
    private InchesHgToMilimetersHgAdapter inchesHgToMilimetersHgAdapter;
    @Autowired
    private MileToKilometerAdapter mileToKilometerAdapter;
    @Autowired
    private MphToKphAdapter mphToKphAdapter;
    @Autowired
    private Validations validations;
    @Autowired
    private WeatherService weatherService;
    @Resource
    private ClientYahooWeather clientYahooWeather;

    public ProxyWeather() {
    }
    
    public ProxyWeather(ClientYahooWeather clientYahooWeather) {
        this.clientYahooWeather = clientYahooWeather;
    }

    public ProxyWeather(WeatherService weatherService) {
        this.weatherService = weatherService;
    }
    
    @Override
    public String getWeather(String queryConsult, String format, String env) {
        try {
            String response = clientYahooWeather.getWeather(queryConsult, format, env);
            return response;            
        } catch (Exception ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public String getWeatherResponse(String country, String city) {
        try {
            if (verifyInternetConnection()) {
                String[] queryConsult = makeQueryConsult(country, city);
                String clientYahooWeatherString = getWeather(queryConsult[0], queryConsult[1], queryConsult[2]);
                String response = responseMapper(clientYahooWeatherString);
                return response;
            } else {
                String response = "";
                for (Day day : getDaysList()) {
                    response += day.toString();
                }
                return response;
            }
        } catch (Exception ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public boolean verifyInternetConnection(){
        try{
            InetAddress address = InetAddress.getByName("www.google.com");
            if (address != null) {
                return true;
            }             
        }catch (UnknownHostException ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return false;
    }

    public String responseMapper(String response) {
        String res = "";
        try {
            ObjectMapper defaultObjectMapper = new ObjectMapper();
            JsonNode jNode = defaultObjectMapper.readTree(response);
            JsonNode nodeToMap = jNode.get("query").get("results").get("channel");
            List<Day> dayList = ForecastTransformer.transformJsonNodeToDaysList(nodeToMap);
            dayList = adaptValues(dayList);
            for (Day d : dayList) {
                weatherService.updateDay(d);
                res += d.toString();
            }
            return res;
        } catch (IOException ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String[] makeQueryConsult(String country, String city) {
        String[] res = new String[3];
        String yahooQuery = "select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%2C%20" + country + "%22)";
        String format = "json";
        String env = "store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        res[0] = yahooQuery;
        res[1] = format;
        res[2] = env;
        return res;
    }

    public List<Day> adaptValues(List<Day> daysList) {
        try {
            for (Day d : daysList) {
                d.getTemp().setTemp(String.valueOf(farenheitToCelciusAdapter.getCelciusTemperature(Double.parseDouble(d.getTemp().getTemp()))));
                d.getTemp().setHigh(String.valueOf(farenheitToCelciusAdapter.getCelciusTemperature(Double.parseDouble(d.getTemp().getHigh()))));
                d.getTemp().setLow(String.valueOf(farenheitToCelciusAdapter.getCelciusTemperature(Double.parseDouble(d.getTemp().getLow()))));
                d.getAtmosphere().setVisibility(String.valueOf(mileToKilometerAdapter.getKilometer(Double.parseDouble(d.getAtmosphere().getVisibility()))));
                d.getAtmosphere().setPressure(String.valueOf(inchesHgToMilimetersHgAdapter.getMilimetersHg(Double.parseDouble(d.getAtmosphere().getPressure()))));
                d.getWind().setSpeed(String.valueOf(mphToKphAdapter.getKphSpeed(Double.parseDouble(d.getWind().getSpeed()))));
            }
            return daysList;
        } catch (Exception ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Day> getDaysList() {
        try {
            if (!validations.validateForNull()) {
                List<Day> dayList = weatherService.getAllDay();
                dayList = adaptValues(dayList);
                return dayList;
            }
        } catch (Exception ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Day> updateDay(Day day) {
        try {
            weatherService.updateDay(day);
            List<Day> dayList = weatherService.getAllDay();
            dayList = adaptValues(dayList);
            return dayList;
        } catch (Exception ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Day> addDay(Day day) {
        try {
            weatherService.insertDay(day);
            return adaptValues(weatherService.getAllDay());
        } catch (Exception ex) {
            Logger.getLogger(ProxyWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    

}
