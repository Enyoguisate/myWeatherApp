package com.weatherapp.maven.entitymodels;

public class Day implements Cloneable {

    private int id;
    private Date date;
    private Temperature temp;
    private String conditionDescription;
    private Wind wind;
    private Atmosphere atmosphere;
    private Astronomy astronomy;
    private Location location;

    public Day(int id, Date date, Temperature temp, String conditionDescription, Wind wind, Atmosphere atmosphere, Astronomy astronomy, Location location) {
        this.id = id;
        this.date = date;
        this.temp = temp;
        this.conditionDescription = conditionDescription;
        this.wind = wind;
        this.atmosphere = atmosphere;
        this.astronomy = astronomy;
        this.location = location;
    }

    public Day() {
        this.date = null;
        this.temp = null;
        this.conditionDescription = "";
        this.wind = null;
        this.atmosphere = null;
        this.astronomy = null;
        this.location = null;
        this.id = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Temperature getTemp() {
        return temp;
    }

    public void setTemp(Temperature temp) {
        this.temp = temp;
    }

    public String getConditionDescription() {
        return conditionDescription;
    }

    public void setConditionDescription(String conditionDescription) {
        this.conditionDescription = conditionDescription;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Atmosphere getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(Atmosphere atmosphere) {
        this.atmosphere = atmosphere;
    }

    public Astronomy getAstronomy() {
        return astronomy;
    }

    public void setAstronomy(Astronomy astronomy) {
        this.astronomy = astronomy;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "\n { Lugar: " + location.getCountry() + ", "
                + location.getRegion() + ", "
                + location.getCity() + " \n"
                + "Dia: " + date.getDay() + " \n"
                + "Fecha: " + date.getDate() + " \n"
                + "Temperatura: " + temp.getTemp() + " C \n"
                + "Maxima:" + temp.getHigh() + " C \n"
                + "Minima: " + temp.getLow() + " C \n"
                + "Condicion: " + conditionDescription + " \n"
                + "Viento: " + wind.getSpeed() + " Kph " + ", Direccion: " + wind.getDirection() + " \n"
                + "Humedad: " + atmosphere.getHumidity() + " % \n"
                + "Presion: " + atmosphere.getPressure() + " mmHg \n"
                + "Visibilidad: " + atmosphere.getVisibility() + " Km \n"
                + "Amanecer: " + astronomy.getSunrise() + " \n"
                + "Atardecer: " + astronomy.getSunset() + " \n }";

    }

}
