package com.weatherapp.maven.entitymodels;

public class Atmosphere {

    private int id;
    private String humidity;
    private String pressure;
    private String visibility;

    public Atmosphere(int id, String humidity, String pressure, String visibility) {
        this.humidity = humidity;
        this.pressure = pressure;
        this.visibility = visibility;
        this.id = id;
    }

    public Atmosphere() {
        this.humidity = "";
        this.pressure = "";
        this.visibility = "";
        this.id = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    @Override
    public String toString() {
        return " Datos Atmosfericos: \n"
                + "Humedad: " + humidity + " % \n"
                + "Presión: " + pressure + " mmHg \n"
                + "Visibilidad: " + visibility + " Km \n";
    }

}
