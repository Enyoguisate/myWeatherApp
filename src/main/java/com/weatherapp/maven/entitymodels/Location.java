package com.weatherapp.maven.entitymodels;

public class Location {

    private int id;
    private String city;
    private String country;
    private String region;
    private String lat;
    private String lon;

    public Location(int id, String city, String country, String region, String lat, String lon) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.region = region;
        this.lat = lat;
        this.lon = lon;

    }

    public Location() {
        this.city = "";
        this.country = "";
        this.region = "";
        this.lat = "";
        this.lon = "";
        this.id = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "\nLocalizacion:\n"
                + "Ciudad: " + city + "\n"
                + "Pais: " + country + "\n"
                + "Region: " + region + "\n"
                + "Latitud: " + lat + "°\n"
                + "Longitud: " + lon + "°\n";

    }

}
