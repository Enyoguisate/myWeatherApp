package com.weatherapp.maven.entitymodels;

public class Date {

    private int id;
    private String date;
    private String day;

    public Date(int id, String date, String day) {
        this.id = id;
        this.date = date;
        this.day = day;
    }

    public Date() {
        this.id = 0;
        this.date = "";
        this.day = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return " Fecha: " + date + " \n"
                + "Dia: " + day + " \n";
    }

}
