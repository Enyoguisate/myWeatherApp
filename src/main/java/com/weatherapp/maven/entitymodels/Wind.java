
package com.weatherapp.maven.entitymodels;


public class Wind {
    private int id;
    private String direction;
    private String speed;
    
    
    public Wind(int id, String direction, String speed) {
        this.id = id;
        this.direction = direction;
        this.speed = speed;                
    }

    public Wind() {
        this.id = 0;
        this.direction = "";
        this.speed = "";        
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Viento: \n" 
                + "Direccion: " + direction +" \n"
                + "Velocidad: " + speed +" Kph \n";
    }

    
   
    
}
