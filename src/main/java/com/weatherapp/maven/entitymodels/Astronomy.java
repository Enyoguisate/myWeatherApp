package com.weatherapp.maven.entitymodels;

public class Astronomy {

    private int id;
    private String sunrise;
    private String sunset;

    public Astronomy(int id, String sunrise, String sunset) {
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.id = id;
    }

    public Astronomy() {
        this.sunrise = "";
        this.sunset = "";
        this.id = 0;
    }

    public Astronomy(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    @Override
    public String toString() {
        return "Datos Astronomicos:\n"
                + "Amanecer: " + sunrise + "\n"
                + "Atardecer: " + sunset + "\n";
    }

}
