package com.weatherapp.maven.entitymodels;

public class Temperature {

    private int id;
    private String temp;
    private String high;
    private String low;

    public Temperature(int id, String temp, String high, String low) {
        this.id = id;
        this.temp = temp;
        this.high = high;
        this.low = low;
    }

    public Temperature() {
        this.id = 0;
        this.temp = temp;
        this.high = high;
        this.low = low;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    @Override
    public String toString() {
        return "Temperatura: " + temp + " C \n"
                + "Maxima: " + high + " C \n"
                + "Minima: " + low + " C \n";
    }

}
