package com.weatherapp.maven.builder;

import com.weatherapp.maven.entitymodels.Astronomy;
import com.weatherapp.maven.entitymodels.Atmosphere;
import com.weatherapp.maven.entitymodels.Date;
import com.weatherapp.maven.entitymodels.Day;
import com.weatherapp.maven.entitymodels.Location;
import com.weatherapp.maven.entitymodels.Temperature;
import com.weatherapp.maven.entitymodels.Wind;

public class DayBuilder {

    private int id;
    private Date date;
    private Temperature temp;
    private String conditionDescription;
    private Wind wind;
    private Atmosphere atmosphere;
    private Astronomy astronomy;
    private Location location;
    private Day _day;

    public DayBuilder() {
        this.id = 1;
        this.astronomy = new Astronomy(1, "07:43", "18:40");
        this.atmosphere = new Atmosphere(1, "99", "88", "15");
        this.conditionDescription = "Sunny";
        this.date = new Date(1, "13 May 2017", "Tue");
        this.location = new Location(1, "cordoba", "argentina", "cba", "-66", "-33");
        this.temp = new Temperature(1, "66", "71", "65");
        this.wind = new Wind(1, "NW", "15");
        this._day = new Day(id, date, temp, conditionDescription, wind, atmosphere, astronomy, location);
    }

    public DayBuilder(int id) {
        this.id = id;
        this._day = new Day();
        this._day.setId(this.id);
    }

    public DayBuilder(int id, Date date) {
        this.id = id;
        this.date = new DateBuilder(date.getId(), date.getDate(), date.getDay()).build();
        this._day = new Day();
        this._day.setId(this.id);
        this._day.setDate(this.date);
    }

    public DayBuilder(int id, Date date, Temperature temp) {
        this.id = id;
        this.date = new DateBuilder(date.getId(), date.getDate(), date.getDay()).build();
        this.temp = new TemperatureBuilder(temp.getId(), temp.getTemp(), temp.getHigh(), temp.getLow()).build();
        this._day = new Day();
        this._day.setId(this.id);
        this._day.setDate(this.date);
        this._day.setTemp(this.temp);
    }

    public DayBuilder(int id, Date date, Temperature temp, String conditionDescription) {
        this.id = id;
        this.date = new DateBuilder(date.getId(), date.getDate(), date.getDay()).build();
        this.temp = new TemperatureBuilder(temp.getId(), temp.getTemp(), temp.getHigh(), temp.getLow()).build();
        this.conditionDescription = conditionDescription;
        this._day = new Day();
        this._day.setId(this.id);
        this._day.setDate(this.date);
        this._day.setTemp(this.temp);
        this._day.setConditionDescription(this.conditionDescription);
    }

    public DayBuilder(int id, Date date, Temperature temp, String conditionDescription, Wind wind) {
        this.id = id;
        this.date = new DateBuilder(date.getId(), date.getDate(), date.getDay()).build();
        this.temp = new TemperatureBuilder(temp.getId(), temp.getTemp(), temp.getHigh(), temp.getLow()).build();
        this.conditionDescription = conditionDescription;
        this.wind = new WindBuilder(wind.getId(), wind.getDirection(), wind.getSpeed()).build();
        this._day = new Day();
        this._day.setId(this.id);
        this._day.setDate(this.date);
        this._day.setTemp(this.temp);
        this._day.setConditionDescription(this.conditionDescription);
        this._day.setWind(this.wind);
    }

    public DayBuilder(int id, Date date, Temperature temp, String conditionDescription, Wind wind, Atmosphere atmosphere) {
        this.id = id;
        this.date = new DateBuilder(date.getId(), date.getDate(), date.getDay()).build();
        this.temp = new TemperatureBuilder(temp.getId(), temp.getTemp(), temp.getHigh(), temp.getLow()).build();
        this.conditionDescription = conditionDescription;
        this.wind = new WindBuilder(wind.getId(), wind.getDirection(), wind.getSpeed()).build();
        this.atmosphere = new AtmosphereBuilder(atmosphere.getId(), atmosphere.getHumidity(), atmosphere.getPressure(), atmosphere.getVisibility()).build();
        this._day = new Day();
        this._day.setId(this.id);
        this._day.setDate(this.date);
        this._day.setTemp(this.temp);
        this._day.setConditionDescription(this.conditionDescription);
        this._day.setWind(this.wind);
        this._day.setAtmosphere(this.atmosphere);
    }

    public DayBuilder(int id, Date date, Temperature temp, String conditionDescription, Wind wind, Atmosphere atmosphere, Astronomy astronomy) {
        this.id = id;
        this.date = new DateBuilder(date.getId(), date.getDate(), date.getDay()).build();
        this.temp = new TemperatureBuilder(temp.getId(), temp.getTemp(), temp.getHigh(), temp.getLow()).build();
        this.conditionDescription = conditionDescription;
        this.wind = new WindBuilder(wind.getId(), wind.getDirection(), wind.getSpeed()).build();
        this.atmosphere = new AtmosphereBuilder(atmosphere.getId(), atmosphere.getHumidity(), atmosphere.getPressure(), atmosphere.getVisibility()).build();
        this.astronomy = new AstronomyBuilder(astronomy.getId(), astronomy.getSunrise(), astronomy.getSunset()).build();
        this._day = new Day();
        this._day.setId(this.id);
        this._day.setDate(this.date);
        this._day.setTemp(this.temp);
        this._day.setConditionDescription(this.conditionDescription);
        this._day.setWind(this.wind);
        this._day.setAtmosphere(this.atmosphere);
        this._day.setAstronomy(this.astronomy);
    }

    public DayBuilder(int id, Astronomy astronomy, Atmosphere atmosphere, String conditionDescription, Date date, Location location, Temperature temp, Wind wind) {
        this.id = id;
        this.astronomy = new AstronomyBuilder(astronomy.getId(), astronomy.getSunrise(), astronomy.getSunset()).build();
        this.atmosphere = new AtmosphereBuilder(atmosphere.getId(), atmosphere.getHumidity(), atmosphere.getPressure(), atmosphere.getVisibility()).build();
        this.conditionDescription = conditionDescription;
        this.date = new DateBuilder(date.getId(), date.getDate(), date.getDay()).build();
        this.location = new LocationBuilder(location.getId(), location.getCity(), location.getCountry(), location.getRegion(), location.getLat(), location.getLon()).build();
        this.temp = new TemperatureBuilder(temp.getId(), temp.getTemp(), temp.getHigh(), temp.getLow()).build();
        this.wind = new WindBuilder(wind.getId(), wind.getDirection(), wind.getSpeed()).build();
        this._day = new Day();
        this._day.setId(this.id);
        this._day.setDate(this.date);
        this._day.setTemp(this.temp);
        this._day.setConditionDescription(this.conditionDescription);
        this._day.setWind(this.wind);
        this._day.setAtmosphere(this.atmosphere);
        this._day.setAstronomy(this.astronomy);
        this._day.setLocation(this.location);
    }

    public DayBuilder id(int id) {
        this._day = new Day();
        this._day.setId(id);
        this._day.setAstronomy(this.astronomy);
        this._day.setAtmosphere(this.atmosphere);
        this._day.setConditionDescription(this.conditionDescription);
        this._day.setDate(this.date);
        this._day.setLocation(this.location);
        this._day.setTemp(this.temp);
        this._day.setWind(this.wind);
        return this;
    }

    public DayBuilder astronomy(Astronomy astronomy) {
        this._day.setAstronomy(astronomy);
        return this;
    }

    public DayBuilder atmosphere(Atmosphere atmosphere) {
        this._day.setAtmosphere(atmosphere);
        return this;
    }

    public DayBuilder conditionDescription(String conditionDescription) {
        this._day.setConditionDescription(conditionDescription);
        return this;
    }

    public DayBuilder date(Date date) {
        this._day.setDate(date);
        return this;
    }

    public DayBuilder location(Location location) {
        this._day.setLocation(location);
        return this;
    }

    public DayBuilder temperature(Temperature temperature) {
        this._day.setTemp(temperature);
        return this;
    }

    public DayBuilder wind(Wind wind) {
        this._day.setWind(wind);
        return this;
    }

    public DayBuilder byDefault() {
        AstronomyBuilder astroDef = new AstronomyBuilder();
        AtmosphereBuilder atmoDef = new AtmosphereBuilder(id);
        DateBuilder dateDef = null;
        LocationBuilder locaDef = null;
        TemperatureBuilder tempDef = null;
        WindBuilder windDef = null;
        return this.id(1).astronomy(astroDef.byDefault().build()).atmosphere(atmoDef.byDefault().build()).date(dateDef.byDefault().build()).conditionDescription("Scattered Storms").location(locaDef.byDefault().build()).temperature(tempDef.byDefault().build()).wind(windDef.byDefault().build());
    }

    public Day build() {
        return this._day;
    }

}
