package com.weatherapp.maven.builder;

import com.weatherapp.maven.entitymodels.Temperature;

public class TemperatureBuilder {

    private int id;
    private String temp;
    private String high;
    private String low;
    private Temperature _temperature;

    public TemperatureBuilder() {
        this._temperature = new Temperature(1, "66", "71", "65");
    }

    public TemperatureBuilder(int id) {
        this.id = id;
        this._temperature = new Temperature();
        this._temperature.setId(this.id);
    }

    public TemperatureBuilder(int id, String temp) {
        this.id = id;
        this.temp = temp;
        this._temperature = new Temperature();
        this._temperature.setId(this.id);
        this._temperature.setTemp(this.temp);

    }

    public TemperatureBuilder(int id, String temp, String high) {
        this.id = id;
        this.temp = temp;
        this.high = high;
        this._temperature = new Temperature();
        this._temperature.setId(this.id);
        this._temperature.setTemp(this.temp);
        this._temperature.setHigh(this.high);
    }

    public TemperatureBuilder(int id, String temp, String high, String low) {
        this.id = id;
        this.temp = temp;
        this.high = high;
        this.low = low;
        this._temperature = new Temperature();
        this._temperature.setId(this.id);
        this._temperature.setTemp(this.temp);
        this._temperature.setHigh(this.high);
        this._temperature.setLow(this.low);
    }

    public TemperatureBuilder id(int id) {
        this._temperature = new Temperature();
        this._temperature.setId(id);
        this._temperature.setTemp(temp);
        this._temperature.setHigh(high);
        this._temperature.setLow(low);
        return this;
    }

    public TemperatureBuilder temp(String temp) {
        this._temperature.setTemp(temp);
        return this;
    }

    public TemperatureBuilder high(String high) {
        this._temperature.setHigh(high);
        return this;
    }

    public TemperatureBuilder low(String low) {
        this._temperature.setLow(low);
        return this;
    }

    public TemperatureBuilder byDefault() {
        return this.id(1).temp("66").high("77").low("55");
    }

    public Temperature build() {
        return this._temperature;
    }

}
