package com.weatherapp.maven.builder;

import com.weatherapp.maven.entitymodels.Location;

public class LocationBuilder {

    private int id;
    private String city;
    private String country;
    private String region;
    private String lat;
    private String lon;
    private Location location;

    public LocationBuilder() {
        this.location = new Location(1, "cordoba", "argentina", "cba", "-66", "-33");
    }

    public LocationBuilder(int id) {
        this.id = id;
        this.location = new Location();
        location.setId(this.id);
    }

    public LocationBuilder(int id, String city) {
        this.id = id;
        this.city = city;
        this.location = new Location();
        this.location.setId(this.id);
        this.location.setCity(this.city);
    }

    public LocationBuilder(int id, String city, String country) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.location = new Location();
        this.location.setId(this.id);
        this.location.setCity(this.city);
        this.location.setCountry(this.country);
    }

    public LocationBuilder(int id, String city, String country, String region) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.region = region;
        this.location = new Location();
        this.location.setId(this.id);
        this.location.setCity(this.city);
        this.location.setCountry(this.country);
        this.location.setRegion(this.region);
    }

    public LocationBuilder(int id, String city, String country, String region, String lat) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.region = region;
        this.lat = lat;
        this.location = new Location();
        this.location.setId(this.id);
        this.location.setCity(this.city);
        this.location.setCountry(this.country);
        this.location.setRegion(this.region);
        this.location.setLat(this.lat);
    }

    public LocationBuilder(int id, String city, String country, String region, String lat, String lon) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.region = region;
        this.lat = lat;
        this.lon = lon;
        this.location = new Location();
        this.location.setId(this.id);
        this.location.setCity(this.city);
        this.location.setCountry(this.country);
        this.location.setRegion(this.region);
        this.location.setLat(this.lat);
        this.location.setLon(this.lon);
    }

    public LocationBuilder id(int id) {
        this.location = new Location();
        this.location.setId(id);
        this.location.setCity(city);
        this.location.setCountry(country);
        this.location.setRegion(region);
        this.location.setLat(lat);
        this.location.setLon(lon);
        return this;
    }

    public LocationBuilder city(String city) {
        this.location.setCity(city);
        return this;
    }

    public LocationBuilder country(String country) {
        this.location.setCountry(country);
        return this;
    }

    public LocationBuilder region(String region) {
        this.location.setRegion(region);
        return this;
    }

    public LocationBuilder lat(String lat) {
        this.location.setLat(lat);
        return this;
    }

    public LocationBuilder lon(String lon) {
        this.location.setLon(lon);
        return this;
    }

    public LocationBuilder byDefault() {
        return this.id(1).city("Cordoba").country("Argentina").region("CBA").lat("66").lon("66");
    }

    public Location build() {
        return this.location;
    }

}
