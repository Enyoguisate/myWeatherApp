package com.weatherapp.maven.builder;

import com.weatherapp.maven.entitymodels.Astronomy;

public class AstronomyBuilder {

    private int id;
    private String sunrise;
    private String sunset;
    private Astronomy astronomy;

    public AstronomyBuilder() {
        this.astronomy = new Astronomy(1, "07:43", "18:40");
    }

    public AstronomyBuilder(int id) {
        this.id = id;
        this.astronomy = new Astronomy();
        this.astronomy.setId(this.id);
    }

    public AstronomyBuilder(int id, String sunrise) {
        this.id = id;
        this.sunrise = sunrise;
        this.astronomy = new Astronomy();
        this.astronomy.setId(this.id);
        this.astronomy.setSunrise(this.sunrise);
    }

    public AstronomyBuilder(int id, String sunrise, String sunset) {
        this.id = id;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.astronomy = new Astronomy();
        this.astronomy.setId(this.id);
        this.astronomy.setSunrise(this.sunrise);
        this.astronomy.setSunset(this.sunset);
    }

    public AstronomyBuilder id(int id) {
        this.astronomy = new Astronomy(id, sunrise, sunset);
        return this;
    }

    public AstronomyBuilder sunrise(String sunrise) {
        this.astronomy.setSunrise(sunrise);
        return this;
    }

    public AstronomyBuilder sunset(String sunset) {
        this.astronomy.setSunset(sunset);
        return this;
    }

    public AstronomyBuilder byDefault() {
        return this.id(1).sunrise("07:43").sunset("18:40");
    }

    public Astronomy build() {
        return this.astronomy;
    }

}
