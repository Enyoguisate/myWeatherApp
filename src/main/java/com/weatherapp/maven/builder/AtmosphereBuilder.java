package com.weatherapp.maven.builder;

import com.weatherapp.maven.entitymodels.Atmosphere;

public class AtmosphereBuilder {

    private int id;
    private String humidity;
    private String pressure;
    private String visibility;
    private Atmosphere atmosphere;

    public AtmosphereBuilder() {
        this.atmosphere = new Atmosphere(1, "99", "88", "15");
    }

    public AtmosphereBuilder(int id) {
        this.id = id;
        this.atmosphere = new Atmosphere();
        atmosphere.setId(this.id);
    }

    public AtmosphereBuilder(int id, String humidity) {
        this.id = id;
        this.humidity = humidity;
        this.atmosphere = new Atmosphere();
        this.atmosphere.setId(this.id);
        this.atmosphere.setHumidity(this.humidity);
    }

    public AtmosphereBuilder(int id, String humidity, String pressure) {
        this.id = id;
        this.humidity = humidity;
        this.pressure = pressure;
        this.atmosphere = new Atmosphere();
        this.atmosphere.setId(this.id);
        this.atmosphere.setHumidity(this.humidity);
        this.atmosphere.setPressure(this.pressure);
    }

    public AtmosphereBuilder(int id, String humidity, String pressure, String visibility) {
        this.id = id;
        this.humidity = humidity;
        this.pressure = pressure;
        this.visibility = visibility;
        this.atmosphere = new Atmosphere();
        this.atmosphere.setId(this.id);
        this.atmosphere.setHumidity(this.humidity);
        this.atmosphere.setPressure(this.pressure);
        this.atmosphere.setVisibility(this.visibility);
    }

    public AtmosphereBuilder id(int id) {
        this.atmosphere = new Atmosphere();
        this.atmosphere.setId(id);
        this.atmosphere.setHumidity(humidity);
        this.atmosphere.setPressure(pressure);
        this.atmosphere.setVisibility(visibility);
        return this;
    }

    public AtmosphereBuilder humidity(String humidity) {
        this.atmosphere.setHumidity(humidity);
        return this;
    }

    public AtmosphereBuilder pressure(String pressure) {
        this.atmosphere.setPressure(pressure);
        return this;
    }

    public AtmosphereBuilder visibility(String visibility) {
        this.atmosphere.setVisibility(visibility);
        return this;
    }

    public AtmosphereBuilder byDefault() {
        return this.id(1).humidity("99").pressure("88").visibility("15");
    }

    public Atmosphere build() {
        return this.atmosphere;
    }

}
