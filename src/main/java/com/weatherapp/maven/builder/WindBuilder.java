package com.weatherapp.maven.builder;

import com.weatherapp.maven.entitymodels.Wind;

public class WindBuilder {

    private int id;
    private String direction;
    private String speed;
    private Wind _wind;

    public WindBuilder() {
        this._wind = new Wind(1, "NW", "15");
    }

    public WindBuilder(int id) {
        this.id = id;
        this._wind = new Wind();
        this._wind.setId(this.id);
    }

    public WindBuilder(int id, String direction) {
        this.id = id;
        this.direction = direction;
        this._wind = new Wind();
        this._wind.setId(this.id);
        this._wind.setDirection(this.direction);
    }

    public WindBuilder(int id, String direction, String speed) {
        this.id = id;
        this.direction = direction;
        this.speed = speed;
        this._wind = new Wind();
        this._wind.setId(this.id);
        this._wind.setDirection(this.direction);
        this._wind.setSpeed(this.speed);
    }

    public WindBuilder id(int id) {
        this._wind = new Wind();
        this._wind.setId(id);
        this._wind.setDirection(direction);
        this._wind.setSpeed(speed);
        return this;
    }

    public WindBuilder direction(String direction) {
        this._wind.setDirection(direction);
        return this;
    }

    public WindBuilder speed(String speed) {
        this._wind.setSpeed(speed);
        return this;
    }

    public WindBuilder byDefault() {
        return this.id(1).direction("NO").speed("15..3");
    }

    public Wind build() {
        return this._wind;
    }

}
