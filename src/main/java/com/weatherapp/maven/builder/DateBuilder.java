package com.weatherapp.maven.builder;

import com.weatherapp.maven.entitymodels.Date;

public class DateBuilder {

    private int id;
    private String date;
    private String day;
    private Date _date;

    public DateBuilder() {
        this._date = new Date(1, "13 May 2017", "Tue");
    }

    public DateBuilder(int id) {
        this.id = id;
        this._date = new Date();
        this._date.setId(this.id);
    }

    public DateBuilder(int id, String date) {
        this.id = id;
        this.date = date;
        this._date = new Date();
        this._date.setId(this.id);
        this._date.setDate(this.date);
    }

    public DateBuilder(int id, String date, String day) {
        this.id = id;
        this.date = date;
        this.day = day;
        this._date = new Date();
        this._date.setId(this.id);
        this._date.setDate(this.date);
        this._date.setDay(this.day);
    }

    public DateBuilder id(int id) {
        this._date = new Date();
        this._date.setId(id);
        this._date.setDate(this.date);
        this._date.setDay(this.day);
        return this;
    }

    public DateBuilder date(String date) {
        this._date.setDate(date);
        return this;
    }

    public DateBuilder day(String day) {
        this._date.setDay(day);
        return this;
    }

    public DateBuilder byDefault() {
        return this.id(1).date("05 May 2017").day("Tue");
    }

    public Date build() {
        return this._date;
    }

}
